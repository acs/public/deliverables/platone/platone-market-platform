// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'
import App from './App'
import router from './router'
import VueApexCharts from 'vue-apexcharts'
import Datetime from 'vue-datetime'
import 'vue-datetime/dist/vue-datetime.css'
import moment from 'moment'
import VueSession from 'vue-session'
import JsonExcel from 'vue-json-excel'
import VueRangedatePicker from 'vue-rangedate-picker'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.component('v-icon', Icon)
Vue.component('apexchart', VueApexCharts)
Vue.component('downloadExcel', JsonExcel)
Vue.component('rangedate-picker', VueRangedatePicker)
Vue.use(Datetime)
Vue.use(VueSession)
Vue.use(VueRangedatePicker)
// You need a specific loader for CSS files

Vue.filter('formatDate', function (value) {
  if (value) {
    return moment(String(value)).format('DD/MM/YYYY HH:mm')
  }
})
Vue.filter('formatDatePeriod', function (value) {
  if (value) {
    return moment(String(value)).format('DD/MM/YYYY')
  }
})
Vue.filter('formatNumber', function (value) {
  if (value) {
    return value.toString().replace('.', ',')
  }
})
Vue.filter('formatKw', function (value) {
  if (value) {
    return value.toString().concat(' kW')
  }
})
Vue.filter('formatQKw', function (value) {
  if (value) {
    return value.toString().concat(' kVAr')
  }
})
Vue.filter('formatPrice', function (value) {
  if (value) {
    return value.toString().concat(' €')
  }
})
Vue.filter('formatPriceKw', function (value) {
  if (value) {
    return value.toString().concat(' €/kWh')
  }
})
Vue.filter('formatQPriceKw', function (value) {
  if (value) {
    return value.toString().concat(' €/kVArh')
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
