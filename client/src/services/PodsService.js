import Api from '@/services/Api'

export default {
  getPods () {
    return Api().get('podRegistry/')
  }
}
