import Api from '@/services/Api'

export default {
  getByMarketSessions (marketSessionId) {
    return Api().get('validatedOutcome/market-session/' + marketSessionId)
  }
}
