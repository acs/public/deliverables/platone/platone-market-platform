import Api from '@/services/Api'

export default {
  getByMarketSessions (marketSessionId) {
    return Api().get('settlement/market-session/' + marketSessionId)
  }
}
