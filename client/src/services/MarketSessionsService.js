import Api from '@/services/Api'

export default {
  getMarketSessions (params) {
    return Api().get('marketSession', {params})
  },
  save (params) {
    return Api().post('marketSession', params)
  }
}
