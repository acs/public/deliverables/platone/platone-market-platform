import Api from '@/services/Api'

export default {
  getByMarketSessions (marketSessionId) {
    return Api().get('technicalOutcome/market-session/' + marketSessionId)
  }
}
