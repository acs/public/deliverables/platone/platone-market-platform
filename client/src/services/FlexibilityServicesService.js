import Api from '@/services/Api'

export default {
  getByMarketSessions (marketSessionId) {
    return Api().get('flexibilityService/market-session/' + marketSessionId)
  }
}
