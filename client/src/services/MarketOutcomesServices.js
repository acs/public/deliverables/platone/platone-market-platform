import Api from '@/services/Api'

export default {
  getByMarketSessions (marketSessionId) {
    return Api().get('marketOutcome/market-session/' + marketSessionId)
  }
}
