import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Signup from '@/components/Signup'
import Home from '@/components/Home'
import FlexibilityServices from '@/components/FlexibilityServices'
import MarketSessions from '@/components/MarketSessions'
import Pods from '@/components/Pods'
import EnlintDemo from '@/components/EnlintDemo'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup
    },
    {
      path: '/home',
      name: 'Home',
      component: Home,
      meta: {
        requiresSession: true

      }
    },
    {
      path: '/flexibility-services/:sessionId',
      name: 'FlexibilityServices',
      component: FlexibilityServices,
      meta: {
        requiresSession: true,
        requiresAdmin: true
      }
    },
    {
      path: '/pods',
      name: 'Pods',
      component: Pods,
      meta: {
        requiresSession: true,
        requiresAdmin: true
      }
    },
    {
      path: '/market-sessions',
      name: 'MarketSessions',
      component: MarketSessions,
      meta: {
        requiresSession: true,
        requiresAdmin: true
      }
    },
    {
      path: '/mols',
      name: 'Mols',
      component: EnlintDemo,
      meta: {
        requiresSession: true,
        requiresAdmin: true
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  const reqSession = to.matched.some(route => route.meta.requiresSession)
  const reqAdmin = to.matched.some(route => route.meta.requiresAdmin)

  if (!reqSession) {
    next()
  } else if (router.app.$session.exists() && !reqAdmin) {
    next()
  } else if (reqAdmin && router.app.$session.get('user').role === 'admin') {
    next()
  } else if (reqAdmin && router.app.$session.get('user').role !== 'admin') {
    next({ name: 'Home' })
  } else {
    next({ name: 'Login' })
  }
})

export default router
