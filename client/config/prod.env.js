'use strict'
module.exports = {
  NODE_ENV: '"production"',
  API: process.env.API_URL || '"https://platone.eng.it:8081"'
}
