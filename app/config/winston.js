// Configuration for the winston logger
const appRoot = require('app-root-path');
const winston = require('winston');

// Definition of the message format for the log
const myFormat = winston.format.printf((info) => `${info.timestamp} ${info.level}: ${info.message}: ${info.modulefilename}`);

// 2 loggers: file and console
const options = {
  file: {
    level: 'info', //  possibile values: error, warn,  info,  verbose,  debug,  silly
    filename: `${appRoot}/logs/validation-system.log`,
    format: winston.format.combine(
      winston.format.timestamp(),
      myFormat,
    ),
    maxsize: 5242880, // 5MB
    maxFiles: 50,
  },
  console: {
    level: 'debug', //  possibile values: error, warn,  info,  verbose,  debug,  silly
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.timestamp(),
      myFormat,
    ),
  },
};

const logger = winston.createLogger({
  transports: [
    new winston.transports.File(options.file),
    new winston.transports.Console(options.console),
  ],
  exitOnError: false, // do not exit on handled exceptions
});
logger.warn('Logger created, LOG_LEVEL= info', { modulefilename: __filename });

module.exports = logger;
