const routes = require('express').Router();

const auth = require('./auth');
const oauth = require('./oauth');
const contracts = require('./contracts');
const flexibilityServices = require('./flexibilityService');
const marketOutcomes = require('./marketOutcome');
const marketSessions = require('./marketSession');
const pods = require('./pod');
const podsTso = require('./tso');
const users = require('./user');
const technicalOutcomes = require('./technicalOutcome');
const validatedOutcomes = require('./validatedOutcome');
const wallets = require('./wallet');
const settlement = require('./settlement');
const measurements = require('./measurements');
const smartContracts = require('./smartContract');

const prefix = '/api';

routes.use(`${prefix}`, auth);
routes.use(`${prefix}/oauth/token`, oauth.getAuthToken);
routes.use(`${prefix}/contracts`, oauth.authRequest, contracts);
routes.use(`${prefix}/flexibilityService`, flexibilityServices);
routes.use(`${prefix}/marketOutcome`, marketOutcomes);
routes.use(`${prefix}/marketSession`, marketSessions);
routes.use(`${prefix}/podRegistry`, pods);
routes.use(`${prefix}/tso/pods`, podsTso);
routes.use(`${prefix}/users`, oauth.authRequest, users);
routes.use(`${prefix}/validatedOutcome`, validatedOutcomes);
routes.use(`${prefix}/technicalOutcome`, technicalOutcomes);
routes.use(`${prefix}/wallets`, oauth.authRequest, wallets);
routes.use(`${prefix}/measurements`, measurements);
routes.use(`${prefix}/settlement`, settlement);
routes.use(`${prefix}/smartContracts`, oauth.authRequest, smartContracts);

module.exports = routes;
