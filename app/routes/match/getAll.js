const libs = require('../../libs/match');

module.exports = (req, res) => {
  libs.getAll()
    .then((requests) => res.json(requests))
    .catch((err) => res.status(500).json(err));
};
