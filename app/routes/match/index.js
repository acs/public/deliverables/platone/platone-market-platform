const matches = require('express').Router();

const getAll = require('./getAll');
const get = require('./get');
const save = require('./save');

matches.get('/:id', get);
matches.get('/', getAll);
matches.post('/', save);

module.exports = matches;
