const libs = require('../../libs/tso');

module.exports = (req, res) => {
  libs.getAll()
    .then((podsTso) => res.json(podsTso))
    .catch((err) => res.status(500).json(err));
};
