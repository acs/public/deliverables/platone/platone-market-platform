const libs = require('../../libs/tso');

module.exports = (req, res) => {
  console.log('DELETE TSO');
  console.log(req.params);
  libs.remove(req.params.pod)
    .then((data) => res.json(data))
    .catch((err) => res.status(500).json(err));
};
