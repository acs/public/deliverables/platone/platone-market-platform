const podsTso = require('express').Router();

const oauth = require('../oauth');
const getAll = require('./getAll');
const get = require('./get');
const create = require('./create');
const update = require('./update');
const remove = require('./remove');

podsTso.get('/:id', get);
podsTso.get('/', getAll);
podsTso.post('/', oauth.authRequest, create);
podsTso.put('/', oauth.authRequest, update);
podsTso.delete('/:pod', oauth.authRequest, remove);

module.exports = podsTso;
