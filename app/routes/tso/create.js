const libs = require('../../libs/tso');

module.exports = (req, res) => {
  console.log('CREATE TSO');
  console.log(req.body);
  libs.create(req.body)
    .then((data) => res.json(data))
    .catch((err) => res.status(500).json(err));
};
