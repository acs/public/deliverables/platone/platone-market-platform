const measurements = require('express').Router();

const getAll = require('./getAll');
const get = require('./get');
const save = require('./save');

measurements.get('/:id', get);
measurements.get('/', getAll);
measurements.post('/', save);

module.exports = measurements;
