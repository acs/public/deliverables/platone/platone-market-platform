const libs = require('../../libs/measurements');

module.exports = (req, res) => {
  libs.getAll()
    .then((measurements) => res.json(measurements))
    .catch((err) => res.status(500).json(err));
};
