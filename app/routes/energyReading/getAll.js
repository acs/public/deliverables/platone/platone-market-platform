const libs = require('../../libs/energyReading');

module.exports = (req, res) => {
  libs.getAll()
    .then((energyReadings) => res.json(energyReadings))
    .catch((err) => res.status(500).json(err));
};
