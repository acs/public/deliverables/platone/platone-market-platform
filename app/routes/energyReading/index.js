const energyReadings = require('express').Router();

const getAll = require('./getAll');
const get = require('./get');
const save = require('./save');

energyReadings.get('/:id', get);
energyReadings.get('/', getAll);
energyReadings.post('/', save);

module.exports = energyReadings;
