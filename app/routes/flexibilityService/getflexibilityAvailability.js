const FlexibilityService = require('../../services/flexibilityService');

module.exports = (req, res) => {
  const marketSessionID = req.params.marketSessionId;
  console.log(`PARAMS >> ${req.params.marketSessionId}`);
  FlexibilityService.flexibilityAvailabilityService(marketSessionID).then((result) => {
    res.json(result);
  }).catch((err) => res.json(err));
};
