const flexibilityServiceService = require("../../services/flexibilityService");
const marketSessionLibs = require("../../libs/marketSession");
const flexibilityServiceLibs = require("../../libs/flexibilityService");
const logLibs = require("../../libs/log");

module.exports = async (req, res) => {
  // insert log in mongo messaage received reference=playerServiceId flow 2

  const logParams = {};
  logParams.type = "message_received";
  logParams.flow = "flow2";
  logParams.platform = "MP";
  logParams.referenceId = req.body.playerServiceId;
  logParams.dateTime = new Date();

  logLibs.save(logParams);

  const newFlexibilityService = req.body;

  if (
    !req.body.marketType
    || !req.body.playerId
    || !req.body.playerServiceId
    || !req.body.serviceType
  ) {
    res.status(400).send({ message: "Missing parameter!" });
  }
  const marketSession = await marketSessionLibs.getByType(req.body.marketType);
  const flexibilityServices = await flexibilityServiceLibs.get({
    playerServiceId: newFlexibilityService.playerServiceId,
  });

  if (!marketSession) {
    res.status(400).send({ message: "There is no active marketSession!" });
  }

  if (flexibilityServices && flexibilityServices.length > 0) {
    console.log("UPDATE");
    await flexibilityServiceService.updateFlexibilityService(
      flexibilityServices[0],
    );
  }
  newFlexibilityService.marketSession = marketSession._id;

  // insert log in mongo start_execution
  logParams.type = "start_execution";
  logLibs.save(logParams);

  flexibilityServiceService
    .createFlexibilityService(newFlexibilityService)
    .then((data) => {
      console.log(`DATA  ${data}`);
      // insert log in mongo end_execution
      // insert log in mongo message_sent

      logParams.type = "end_execution";
      logLibs.save(logParams);

      logParams.type = "message_sent";
      logLibs.save(logParams);

      res.json(data);
    })
    .catch((err) => {
      console.log(`ERROR  ${err}`);
      res.status(err.status).send({ message: err.message });
    });
};
