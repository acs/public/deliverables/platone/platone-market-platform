const mongoose = require('mongoose');
const libs = require('../../libs/flexibilityService');

module.exports = (req, res) => {
  const { id } = req.params;

  if (mongoose.Types.ObjectId.isValid(id)) {
    libs.get({ _id: id })
      .then((data) => res.json(data))
      .catch((error) => res.status(500).json(error));
  } else {
    res.status(400).json('missing parameter');
  }
};
