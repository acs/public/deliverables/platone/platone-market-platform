const libs = require('../../libs/flexibilityService');

module.exports = (req, res) => {
  libs.getAll()
    .then((flexibilityServices) => res.json(flexibilityServices))
    .catch((err) => res.status(500).json(err));
};
