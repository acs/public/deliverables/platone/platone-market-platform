const libs = require('../../libs/flexibilityService');

module.exports = (req, res) => {
  console.log(req.params);
  console.log('GET FLEXIBILITY SERVICES');
  libs.get({ marketSession: req.params.marketSessionId })
    .then((flexibilityServices) => res.json(flexibilityServices))
    .catch((err) => res.status(500).json(err));
};
