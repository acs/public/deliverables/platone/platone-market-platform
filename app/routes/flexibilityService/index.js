const flexibilityServices = require('express').Router();

const oauth = require('../oauth');
const getAll = require('./getAll');
const get = require('./get');
const save = require('./save');
const getByMarketSession = require('./getByMarketSession');
const getflexibilityAvailability = require('./getflexibilityAvailability');

flexibilityServices.get('/market-session/:marketSessionId', getByMarketSession);
flexibilityServices.get('/:id', get);
flexibilityServices.get('/', getAll);
flexibilityServices.get('/getflexibilityAvailability/:marketSessionId', getflexibilityAvailability);
flexibilityServices.post('/', oauth.authRequest, save);

module.exports = flexibilityServices;
