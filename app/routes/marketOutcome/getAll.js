const libs = require('../../libs/marketOutcome');

module.exports = (req, res) => {
  libs.getAll()
    .then((marketOutcomes) => res.json(marketOutcomes))
    .catch((err) => res.status(500).json(err));
};
