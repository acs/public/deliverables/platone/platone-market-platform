const libs = require('../../libs/marketOutcome');

module.exports = (req, res) => {
  libs.get({ marketSession: req.params.marketSessionId })
    .then((marketOutcome) => res.json(marketOutcome))
    .catch((err) => res.status(500).json(err));
};
