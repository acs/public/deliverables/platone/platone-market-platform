const marketOutcomes = require('express').Router();

const getAll = require('./getAll');
const get = require('./get');
const save = require('./save');
const getByMarketSession = require('./getByMarketSession');

marketOutcomes.get('/market-session/:marketSessionId', getByMarketSession);
marketOutcomes.get('/:id', get);
marketOutcomes.get('/', getAll);
marketOutcomes.post('/', save);

module.exports = marketOutcomes;
