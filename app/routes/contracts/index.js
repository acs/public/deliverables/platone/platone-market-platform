const contracts = require('express').Router();
const getLettureEnergetiche = require('./energyReadings');
const getTransazioniEconomiche = require('./transactions');

contracts.use('/readings', getLettureEnergetiche);
contracts.use('/transactions', getTransazioniEconomiche);

module.exports = contracts;
