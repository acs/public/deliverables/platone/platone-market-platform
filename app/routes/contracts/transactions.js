const router = require('express').Router();
const podLib = require('../../libs/pod');

const web3 = require('../../config/utils').init;
const tokenContract = require('../../services/contracts').token(web3);

const tokenContractAddress = tokenContract.getAddress();

router.get('/balanceOf/:pod', async (req, res) => {
  const pod = await podLib.get({ pod: req.params.pod });
  const addressWallet = pod[0].wallet;
  tokenContract.connect(tokenContractAddress).then(() => {
    tokenContract.balanceOf(addressWallet).then((result) => {
      res.json({ address: addressWallet, balance: result.toString() });
    }).catch((err) => res.json(err));
  }).catch((err) => res.json(err));
});
/*
router.post('/transaction', (req, res) => {
  const owner = req.body.owner
  const seller = req.body.seller
  const buyer = req.body.buyer
  const kwh = req.body.kwh
  const rate = req.body.rate
  const start = req.body.start
  const end = req.body.end
  contract.connect(contractAddress).then(() => {
    contract.registerTransaction(owner, seller, buyer, kwh, rate, start, end).then(result => {
      res.json(JSON.stringify(result))
    }).catch(err => {
      console.log(err)
      res.json(err)
    })
  }).catch(err => res.json(err))
})

router.get('/transaction/:id', (req, res) => {
  contract.connect(contractAddress).then(() => {
    contract.getSale(req.params.id).then(result => {
      res.json(JSON.stringify(result))
    }).catch(err => res.json(err))
  }).catch(err => res.json(err))
})

router.post('/buyCoins', (req, res) => {
  contract.connect(contractAddress).then(() => {
    contract.buyCoins(req.body.owner, req.body.receiver, req.body.amount).then(result => {
      res.json(JSON.stringify(result))
    }).catch(err => res.json(err))
  }).catch(err => res.json(err))
})

router.get('/coinBalance/:owner', (req, res) => {
  contract.connect(contractAddress).then(() => {
    contract.getCoinBalance(req.params.owner).then(result => {
      res.json(JSON.stringify(result))
    }).catch(err => res.json(err))
  }).catch(err => res.json(err))
})

router.post('/wallets/', (req, res) => {
  web3.eth.personal.sendTransaction({from: req.body.owner, to: req.body.address, value: 1000000000000000000}).then(result => {
    res.json(result)
  }).catch(err => {
    res.json(err)
  })
})

router.get('/wallets/:address', (req, res) => {
  web3.eth.getBalance(req.params.address).then(result => {
    res.json(result)
  }).catch(err => {
    res.json(err)
  })
})
*/
module.exports = router;
