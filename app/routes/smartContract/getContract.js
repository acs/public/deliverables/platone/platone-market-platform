const web3 = require('../../config/utils').init;
const contract = require('../../services/contracts').settlement(web3);

module.exports = (req, res) => {
  const { contractAddress } = req.params;
  console.log(contractAddress);
  contract.connect(contractAddress).then(() => {
    contract.getSettlement().then((result) => {
      res.json(result);
    }).catch((err) => res.json(err));
  }).catch((err) => res.json(err));
};
