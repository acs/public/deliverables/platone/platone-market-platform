const libs = require('../../libs/smartContract');
const web3 = require('../../config/utils').init;
const contract = require('../../services/contracts').settlement(web3);

const contractAddress = contract.getAddress();

const owner = '0xef46e8cafe1b76c92c13824cea5e2e7b1301284e';

module.exports = (req, res) => {
  const timeframes = req.body;
  console.log(`contractAddress${contractAddress}`);
  contract.connect(contractAddress).then(() => {
    contract.deploy(owner, timeframes[0], timeframes[1], timeframes[2], timeframes[3], timeframes[4], timeframes[5], timeframes[6]).then((result) => {
      console.log(`RESULT${result}`);
      const smartContract = {
        address: result,
        owner,
      };
      libs.save(smartContract)
        .then((data) => res.json(data))
        .catch((err) => res.status(500).json(err));
    }).catch((err) => res.json(err));
  }).catch((err) => res.json(err));
};
