const web3 = require('../../config/utils').init;
const contract = require('../../services/contracts').token(web3);

const contractAddress = contract.getAddress();

module.exports = (req, res) => {
  contract.connect(contractAddress).then(() => {
    contract.balanceOf(req.params.address).then((result) => {
      res.json(Number(result));
    }).catch((err) => res.json(err));
  }).catch((err) => res.json(err));
};
