const smartContract = require('express').Router();

const getAll = require('./getAll');
const get = require('./get');
const save = require('./save');
const getContract = require('./getContract');
const getBalance = require('./getBalance');

smartContract.get('/balance/:address', getBalance);
smartContract.get('/contract/:contractAddress', getContract);
smartContract.get('/:id', get);
smartContract.get('/', getAll);
smartContract.post('/', save);

module.exports = smartContract;
