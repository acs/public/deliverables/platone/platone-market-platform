const libs = require('../../libs/smartContract');

module.exports = (req, res) => {
  libs.getAll()
    .then((smartContract) => res.json(smartContract))
    .catch((err) => res.status(500).json(err));
};
