const marketSessions = require('express').Router();

const getAll = require('./getAll');
const get = require('./get');
const save = require('./save');

marketSessions.get('/:id', get);
marketSessions.get('/', getAll);
marketSessions.post('/', save);

module.exports = marketSessions;
