const libs = require('../../libs/marketSession');

module.exports = (req, res) => {
  const query = {};
  if (req.query.start && req.query.end) {
    query.start = { $gte: req.query.start, $lte: req.query.end };
  }
  libs.getAll({ query })
    .then((marketSessions) => res.json(marketSessions))
    .catch((err) => res.status(500).json(err));
};
