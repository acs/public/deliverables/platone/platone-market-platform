const libs = require('../../libs/technicalOutcome');

module.exports = (req, res) => {
  libs.getAll()
    .then((technicalOutcomes) => res.json(technicalOutcomes))
    .catch((err) => res.status(500).json(err));
};
