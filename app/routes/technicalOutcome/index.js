const technicalOutcomes = require('express').Router();

const oauth = require('../oauth');
const getAll = require('./getAll');
const get = require('./get');
const save = require('./save');
const getByMarketSession = require('./getByMarketSession');

technicalOutcomes.get('/market-session/:marketSessionId', getByMarketSession);
technicalOutcomes.get('/:id', get);
technicalOutcomes.get('/', getAll);
technicalOutcomes.post('/', oauth.authRequest, save);

module.exports = technicalOutcomes;
