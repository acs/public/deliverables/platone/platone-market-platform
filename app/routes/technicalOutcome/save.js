const { ObjectId } = require('mongoose').Types;
const libs = require('../../libs/technicalOutcome');
const marketOutcomeLibs = require('../../libs/marketOutcome');
const marketClearingService = require('../../services/marketClearing');
const logLibs = require("../../libs/log");

module.exports = (req, res) => {
  console.log(req.body);
  const technicalOutcome = req.body;

  // insert log in mongo messaage received reference=marketOutcomeId flow 4

  const logParams = {};
  logParams.type = "message_received";
  logParams.flow = "flow4";
  logParams.platform = "MPm";
  logParams.referenceId = req.body.marketOutcome;
  logParams.dateTime = new Date();

  logLibs.save(logParams);

  if (!req.body.marketOutcome || !ObjectId.isValid(req.body.marketOutcome)) {
    res.status(401).json("Market Outcome parameter empty or wrong");
  }

  // insert log in mongo start_execution

  logParams.type = "start_execution";
  logLibs.save(logParams);

  marketOutcomeLibs
    .get({ _id: req.body.marketOutcome })
    .then((marketOutcome) => {
      if (marketOutcome) {
        console.log(`MARKET OUTCOME ->${marketOutcome}`);
        technicalOutcome.marketSession = marketOutcome[0].marketSession;
        libs
          .save(technicalOutcome)
          .then((data) => {
            // insert log in mongo end_execution

            logParams.type = "end_execution";
            logLibs.save(logParams);

            marketClearingService.createValidatedOutcome(technicalOutcome);
            // insert log in mongo message_sent

            logParams.type = "message_sent";
            logLibs.save(logParams);

            res.json(data);
          })
          .catch((err) => res.status(500).json(err));
      } else {
        console.log("Market Outcome does not exists");
        res.status(500).json("Market Outcome does not exists");
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json(err);
    });
};
