const libs = require('../../libs/technicalOutcome');

module.exports = (req, res) => {
  libs.get({ marketSession: req.params.marketSessionId })
    .then((technicalOutcome) => res.json(technicalOutcome))
    .catch((err) => res.status(500).json(err));
};
