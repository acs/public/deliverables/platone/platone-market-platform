const validatedOutcomes = require('express').Router();

const getAll = require('./getAll');
const get = require('./get');
const save = require('./save');
const getByMarketSession = require('./getByMarketSession');

validatedOutcomes.get('/market-session/:marketSessionId', getByMarketSession);
validatedOutcomes.get('/:id', get);
validatedOutcomes.get('/', getAll);
validatedOutcomes.post('/', save);

module.exports = validatedOutcomes;
