const libs = require('../../libs/validatedOutcome');

module.exports = (req, res) => {
  libs.get({ marketSession: req.params.marketSessionId })
    .then((validatedOutcome) => res.json(validatedOutcome))
    .catch((err) => res.status(500).json(err));
};
