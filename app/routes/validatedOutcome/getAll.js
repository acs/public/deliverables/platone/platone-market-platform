const libs = require('../../libs/validatedOutcome');

module.exports = (req, res) => {
  libs.getAll()
    .then((validatedOutcomes) => res.json(validatedOutcomes))
    .catch((err) => res.status(500).json(err));
};
