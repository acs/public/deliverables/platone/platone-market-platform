const libs = require("../../libs/pod");
const logLibs = require("../../libs/log");

module.exports = async (req, res) => {
  const logParams = {};
  logParams.type = "message_received";
  logParams.flow = "flow0";
  logParams.platform = "MP";
  logParams.referenceId = req.body.pod;
  logParams.dateTime = new Date();

  logLibs.save(logParams);

  logParams.type = "start_execution";
  logLibs.save(logParams);

  // insert log in mongo messaage received flow 0 reference = pod
  // insert log in mongo start_execution
  libs
    .create(req.body)
    .then((data) => {
      // insert log in mongo end_execution
      // insert log in mongo message_sent
      logParams.type = "end_execution";
      logLibs.save(logParams);

      logParams.type = "message_sent";
      logLibs.save(logParams);

      res.json(data);
    })
    .catch((err) => res.status(500).json(err));
};
