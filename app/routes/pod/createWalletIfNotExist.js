const libs = require('../../libs/pod');

module.exports = async (req, res) => {
  libs.createWalletIfNotExist(req.body)
    .then((data) => res.json(data))
    .catch((err) => {
      console.error(err);
      res.status(500).json(err.toString());
    });
};
