const pods = require('express').Router();

const oauth = require('../oauth');
const getAll = require('./getAll');
const get = require('./get');
const create = require('./create');
const createWalletIfNotExist = require('./createWalletIfNotExist');
const update = require('./update');
const remove = require('./remove');

pods.get('/:id', get);
pods.get('/', getAll);
pods.post('/', oauth.authRequest, create);
pods.post('/createWalletIfNotExist', oauth.authRequest, createWalletIfNotExist);
pods.put('/', oauth.authRequest, update);
pods.delete('/:pod', oauth.authRequest, remove);

module.exports = pods;
