const libs = require('../../libs/pod');

module.exports = (req, res) => {
  libs.getAll()
    .then((pods) => res.json(pods))
    .catch((err) => res.status(500).json(err));
};
