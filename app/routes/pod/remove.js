const libs = require('../../libs/pod');

module.exports = (req, res) => {
  console.log('CREATE POD');
  console.log(req.params);
  libs.remove(req.params.pod)
    .then((data) => res.json(data))
    .catch((err) => res.status(500).json(err));
};
