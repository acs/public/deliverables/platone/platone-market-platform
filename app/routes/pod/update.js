const libs = require('../../libs/pod');

module.exports = (req, res) => {
  console.log('UPDATE POD');
  console.log(req.body);
  libs.update(req.body)
    .then((data) => res.json(data))
    .catch((err) => res.status(500).json(err));
};
