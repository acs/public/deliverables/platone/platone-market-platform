const OAuth2Server = require('oauth2-server');
const oauthModel = require('../../models/oauth2-model');
const logger = require('../../config/winston');

module.exports = (req, res, next) => {
  const oauth = new OAuth2Server({
    model: oauthModel,
    accessTokenLifetime: 60 * 60,
    allowBearerTokensInQueryString: true,
  });

  const { Request } = OAuth2Server;
  const { Response } = OAuth2Server;

  const request = new Request(req);
  const response = new Response(res);

  return oauth.authenticate(request, response)
    .then(() => {
      // logger.info(`Authenticated request ${request.originalUrl} by  ${JSON.stringify(token)}`, { modulefilename: __filename });
      next();
    }).catch((err) => {
      logger.error(`Error on authenticated request ${request.originalUrl}, error: ${err}`, { modulefilename: __filename });
      res.status(err.code || 500).json(err);
    });
};
