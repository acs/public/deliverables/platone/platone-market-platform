const authRequest = require('./authRequest');
const getAuthToken = require('./getAuthToken');

const self = {
  authRequest,
  getAuthToken,
};

module.exports = self;
