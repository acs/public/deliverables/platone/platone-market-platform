const OAuth2Server = require('oauth2-server');
const oauthModel = require('../../models/oauth2-model');
const logger = require('../../config/winston');

module.exports = (req, res) => {
  req.body.scope = ' ';
  const oauth = new OAuth2Server({
    model: oauthModel,
    accessTokenLifetime: 60 * 60,
    allowBearerTokensInQueryString: true,
  });

  const { Request } = OAuth2Server;
  const { Response } = OAuth2Server;

  const request = new Request(req);
  const response = new Response(res);

  return oauth.token(request, response)
    .then((token) => {
      console.log(token);
      const retToken = {
        access_token: token.accessToken,
        expires_in: token.accessTokenExpiresAt,
        token_type: 'Bearer',
      };
      logger.info(`Request token ${JSON.stringify(token)}`, { modulefilename: __filename });
      res.json(retToken);
    }).catch((err) => {
      logger.error(`Error on obtaining oauth token ${JSON.stringify(err)}`, { modulefilename: __filename });
      res.status(err.code || 500).json(err);
    });
};
