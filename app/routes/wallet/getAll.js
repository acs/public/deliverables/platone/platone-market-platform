const libs = require('../../libs/wallet');

module.exports = (req, res) => {
  libs.getAll()
    .then((wallets) => res.json(wallets))
    .catch((err) => res.status(500).json(err));
};
