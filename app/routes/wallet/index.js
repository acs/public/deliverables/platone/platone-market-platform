const wallets = require('express').Router();

const getAll = require('./getAll');
const get = require('./get');
const save = require('./save');

wallets.get('/:id', get);
wallets.get('/', getAll);
wallets.post('/', save);

module.exports = wallets;
