const libs = require('../../libs/settlement');

module.exports = (req, res) => {
  console.log(`GET SETTLEMENT${req.params}`);
  libs.get({ marketOucome: req.params.marketOucomeId })
    .then((settlement) => res.json(settlement))
    .catch((err) => res.status(500).json(err));
};
