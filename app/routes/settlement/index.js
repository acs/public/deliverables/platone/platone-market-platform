const settlement = require('express').Router();

const oauth = require('../oauth');
const getAll = require('./getAll');
const get = require('./get');
const save = require('./save');
const getByMarketSession = require('./getByMarketSession');
const getByMarketOutcome = require('./getByMarketOutcome');

settlement.get('/market-session/:marketSessionId', getByMarketSession);
settlement.get('/:marketOutcomeId', getByMarketOutcome);
settlement.get('/:id', get);
settlement.get('/', getAll);
settlement.post('/', oauth.authRequest, save);

module.exports = settlement;
