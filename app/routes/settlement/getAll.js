const libs = require('../../libs/settlement');

module.exports = (req, res) => {
  libs.getAll()
    .then((settlement) => res.json(settlement))
    .catch((err) => res.status(500).json(err));
};
