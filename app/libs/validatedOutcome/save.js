const ValidatedOutcome = require('../../models/validatedOutcome');

/**
 * Save an ValidatedOutcome object.
 * @memberOf module:ValidatedOutcome
 * @function save
 * @param {Object} params - ValidatedOutcome params
 *
 * @returns {Promise} - Returns a JSON with the saved ValidatedOutcome
 */

module.exports = (params) => {
  const validatedOutcomeData = new ValidatedOutcome(params);

  return new Promise((resolve, reject) => {
    ValidatedOutcome.findOneAndUpdate({ _id: validatedOutcomeData._id },
      validatedOutcomeData,
      { upsert: true, new: true },
      (err, validatedOutcome) => {
        if (err) {
          reject(err);
        } else {
          resolve(validatedOutcome);
        }
      });
  });
};
