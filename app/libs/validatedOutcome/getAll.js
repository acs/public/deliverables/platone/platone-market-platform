const ValidatedOutcome = require('../../models/validatedOutcome');

/**
 * Get all validatedOutcome
 * @memberOf module:ValidatedOutcome
 * @function getAll
 *
 * @returns {Promise} - Returns a JSON with all validatedOutcome
 */

module.exports = () => new Promise((resolve, reject) => {
  ValidatedOutcome.find({}, (err, validatedOutcomes) => {
    if (err) {
      reject(err);
    } else {
      resolve(validatedOutcomes);
    }
  });
});
