const ValidatedOutcome = require('../../models/validatedOutcome');
const MarketSession = require('../../models/marketSession');

/**
 * Get a specific ValidatedOutcome by id
 * @memberOf module:ValidatedOutcome
 * @function get
 * @param {String} id - a valid ValidatedOutcome id
 *
 * @returns {Promise} - Returns a JSON with the specific ValidatedOutcome
 */

module.exports = (query) => new Promise((resolve, reject) => {
  ValidatedOutcome.find(query).lean().populate('marketSession').exec((err, validatedOutcomes) => {
    if (err) {
      reject(err);
    } else {
      resolve(validatedOutcomes);
    }
  });
});
