/**
 * @module User
 */

const get = require('./get');
const save = require('./save');
const getAll = require('./getAll');
const getByUsername = require('./getByUsername');

const self = {
  get,
  save,
  getAll,
  getByUsername,
};

module.exports = self;
