const User = require('../../models/user');

/**
 * Get a specific user by id
 * @memberOf module:User
 * @function get
 * @param {String} id - a valid user id
 *
 * @returns {Promise} - Returns a JSON with the specific user
 */

module.exports = (id) => new Promise((resolve, reject) => {
  User.find({ _id: id }, (err, users) => {
    if (err) {
      reject(err);
    } else {
      resolve(users);
    }
  });
});
