const MarketOutcome = require('../../models/marketOutcome');

/**
 * Get a specific marketOutcome by id
 * @memberOf module:MarketOutcome
 * @function get
 * @param {String} id - a valid marketOutcome id
 *
 * @returns {Promise} - Returns a JSON with the specific marketOutcome
 */

module.exports = (query) => new Promise((resolve, reject) => {
  MarketOutcome.find(query, (err, marketOutcome) => {
    if (err) {
      reject(err);
    } else {
      resolve(marketOutcome);
    }
  });
});
