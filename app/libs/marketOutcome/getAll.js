const MarketOutcome = require('../../models/marketOutcome');

/**
 * Get all marketOutcome
 * @memberOf module:MarketOutcome
 * @function getAll
 *
 * @returns {Promise} - Returns a JSON with all marketOutcome
 */

module.exports = () => new Promise((resolve, reject) => {
  MarketOutcome.find({}, (err, marketOutcomes) => {
    if (err) {
      reject(err);
    } else {
      resolve(marketOutcomes);
    }
  });
});
