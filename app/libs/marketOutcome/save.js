const MarketOutcome = require('../../models/marketOutcome');

/**
 * Save an MarketOutcome object.
 * @memberOf module:MarketOutcome
 * @function save
 * @param {Object} params - MarketOutcome params
 *
 * @returns {Promise} - Returns a JSON with the saved MarketOutcome
 */

module.exports = (params) => {
  const marketOutcomeData = new MarketOutcome(params);

  return new Promise((resolve, reject) => {
    MarketOutcome.findOneAndUpdate({ _id: marketOutcomeData._id },
      marketOutcomeData,
      { upsert: true, new: true },
      (err, marketOutcome) => {
        if (err) {
          reject(err);
        } else {
          resolve(marketOutcome);
        }
      });
  });
};
