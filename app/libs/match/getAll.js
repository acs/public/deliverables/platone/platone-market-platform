const Match = require('../../models/match');

/**
 * Get all match
 * @memberOf module:Match
 * @function getAll
 *
 * @returns {Promise} - Returns a JSON with all match
 */

module.exports = () => new Promise((resolve, reject) => {
  Match.find({}, (err, matches) => {
    if (err) {
      reject(err);
    } else {
      resolve(matches);
    }
  });
});
