const Match = require('../../models/match');

/**
 * Save an Request object.
 * @memberOf module:Match
 * @function save
 * @param {Object} params - Request params
 *
 * @returns {Promise} - Returns a JSON with the saved match
 */

module.exports = (params) => new Promise((resolve, reject) => {
  Match.findOneAndUpdate(
    { _id: params._id },
    params,
    { upsert: true, new: true },
    (err, match) => {
      if (err) {
        reject(err);
      } else {
        resolve(match);
      }
    },
  );
});
