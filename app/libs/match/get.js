const Match = require('../../models/match');

/**
 * Get a specific match by id
 * @memberOf module:Match
 * @function get
 * @param {String} id - a valid match id
 *
 * @returns {Promise} - Returns a JSON with the specific match
 */

module.exports = (query) => new Promise((resolve, reject) => {
  console.log(query);
  Match.find(query).populate('marketSession').exec((err, match) => {
    if (err) {
      reject(err);
    } else {
      resolve(match);
    }
  });
});
