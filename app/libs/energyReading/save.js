const EnergyReading = require('../../models/energyReading');

/**
 * Save an EnergyReading object.
 * @memberOf module:EnergyReading
 * @function save
 * @param {Object} params - EnergyReading params
 *
 * @returns {Promise} - Returns a JSON with the saved EnergyReading
 */

module.exports = (params) => {
  const energyReadingData = new EnergyReading(params);
  // console.log(`energyReadingData >>>>` + energyReadingData)
  return new Promise((resolve, reject) => {
    EnergyReading.findOneAndUpdate({ _id: energyReadingData._id },
      energyReadingData,
      { upsert: true, new: false },
      (err, energyReading) => {
        if (err) {
          reject(err);
        } else {
          resolve(energyReading);
        }
      });
  });
};
