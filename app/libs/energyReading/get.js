const EnergyReading = require('../../models/energyReading');

/**
 * Get a specific energyReading by id
 * @memberOf module:EnergyReading
 * @function get
 * @param {String} id - a valid energyReading id
 *
 * @returns {Promise} - Returns a JSON with the specific energyReading
 */

module.exports = (id) => new Promise((resolve, reject) => {
  EnergyReading.find({ _id: id }, (err, energyReadings) => {
    if (err) {
      reject(err);
    } else {
      resolve(energyReadings);
    }
  });
});
