const EnergyReading = require('../../models/energyReading');

/**
 * Get all energyReading
 * @memberOf module:EnergyReading
 * @function getAll
 *
 * @returns {Promise} - Returns a JSON with all energyReading
 */

module.exports = () => new Promise((resolve, reject) => {
  EnergyReading.find({}, (err, energyReadings) => {
    if (err) {
      reject(err);
    } else {
      resolve(energyReadings);
    }
  });
});
