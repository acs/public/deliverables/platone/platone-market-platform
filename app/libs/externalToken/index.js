const getNotExpired = require('./getNotExpired');
const update = require('./update');

const self = {
  getNotExpired,
  update,
};

module.exports = self;
