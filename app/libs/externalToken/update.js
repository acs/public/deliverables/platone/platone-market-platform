const ExternalTokenModel = require('../../models/externalToken');

module.exports = (clientID, token, expireDate) => new Promise((resolve, reject) => {
  const update = {
    accessToken: token,
    accessTokenExpiresAt: expireDate,
  };
  ExternalTokenModel.findOneAndUpdate({ clientId: clientID }, update, { new: true, upsert: true },
    (err, newToken) => {
      if (err) {
        reject(err);
      } else resolve(newToken);
    });
});
