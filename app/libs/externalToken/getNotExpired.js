const moment = require('moment');
const ExternalTokenModel = require('../../models/externalToken');

module.exports = (clientID) => new Promise((resolve, reject) => {
  ExternalTokenModel.findOne({ clientId: clientID }, {}, { lean: true },
    (err, token) => {
      if (err) {
        reject(err);
      } else if (!token) {
        resolve({});
      } else {
        const now = moment().utc();
        const expire = moment(token.accessTokenExpiresAt).utc();
        if (now.diff(expire) > 0) {
          resolve({});
        } else resolve(token);
      }
    });
});
