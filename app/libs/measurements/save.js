const Measurements = require("../../models/measurements");
const logLibs = require("../log");

/**
 * Save an Measurements object.
 * @memberOf module:Measurements
 * @function save
 * @param {Object} params - Measurements params
 *
 * @returns {Promise} - Returns a JSON with the saved measurements
 */

module.exports = (params) => new Promise((resolve, reject) => {
  // insert log in mongo start_execution reference = pod + dataTime flow 8

  const logParams = {};
  logParams.type = "start_execution";
  logParams.flow = "flow8";
  logParams.platform = "MP";
  logParams.referenceId = params.pod.concat("-").concat(params.dataTime);
  logParams.dateTime = new Date();

  logLibs.save(logParams);

  Measurements.findOneAndUpdate(
    { dataTime: params.dataTime, pod: params.pod },
    params,
    { upsert: true, new: false },
    (err, measurements) => {
      if (err) {
        reject(err);
      } else {
        // insert log in mongo end_execution reference = pod + dataTime flow 8

        logParams.type = "end_execution";
        logLibs.save(logParams);

        resolve(measurements);
      }
    },
  );
});
