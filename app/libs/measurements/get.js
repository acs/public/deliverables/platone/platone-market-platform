const Measurements = require('../../models/measurements');

/**
 * Get a specific measurements by id
 * @memberOf module:Measurements
 * @function get
 * @param {String} id - a valid measurements id
 *
 * @returns {Promise} - Returns a JSON with the specific measurements
 */

module.exports = (query) => new Promise((resolve, reject) => {
  console.log(query);
  Measurements.find(query, (err, measurements) => {
    if (err) {
      reject(err);
    } else {
      resolve(measurements);
    }
  });
});
