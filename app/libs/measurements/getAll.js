const Measurements = require('../../models/measurements');

/**
 * Get all measurements
 * @memberOf module:Measurements
 * @function getAll
 *
 * @returns {Promise} - Returns a JSON with all measurements
 */

module.exports = () => new Promise((resolve, reject) => {
  Measurements.find({}, (err, measurements) => {
    if (err) {
      reject(err);
    } else {
      resolve(measurements);
    }
  });
});
