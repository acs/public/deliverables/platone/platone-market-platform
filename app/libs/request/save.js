const Request = require('../../models/request');

/**
 * Save an Request object.
 * @memberOf module:Match
 * @function save
 * @param {Object} params - Request params
 *
 * @returns {Promise} - Returns a JSON with the saved match
 */

module.exports = (params) => new Promise((resolve, reject) => {
  console.log('Save requests');
  console.log(params);
  Request.findOneAndUpdate({
    playerServiceId: params.playerServiceId, pod: params.pod, index: params.index, powerType: params.powerType, marketSession: params.marketSession,
  },
  params,
  { upsert: true, new: true },
  (err, request) => {
    if (err) {
      console.log(err)
      reject(err);
    } else {
      resolve(request);
    }
  });
});
