const Request = require('../../models/request');

/**
 * Get all match
 * @memberOf module:Request
 * @function getAll
 *
 * @returns {Promise} - Returns a JSON with all match
 */

module.exports = () => new Promise((resolve, reject) => {
  Request.find({}, (err, requests) => {
    if (err) {
      reject(err);
    } else {
      resolve(requests);
    }
  });
});
