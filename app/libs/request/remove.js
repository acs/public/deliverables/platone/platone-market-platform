const Request = require('../../models/request');

/**
 * Get a specific match by id
 * @memberOf module:Request
 * @function get
 * @param {String} plaierServiceId - a valid playerService id
 *
 * @returns {Promise} - Returns a JSON with the deleted objects
 */

module.exports = (playerServiceId) => new Promise((resolve, reject) => {
  Request.deleteMany({ playerServiceId }).exec((err, data) => {
    if (err) {
      reject(err);
    } else {
      console.log(data);
      resolve('Successful deleted');
    }
  });
});
