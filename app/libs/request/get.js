const Request = require('../../models/request');

/**
 * Get a specific match by id
 * @memberOf module:Match
 * @function get
 * @param {String} id - a valid match id
 *
 * @returns {Promise} - Returns a JSON with the specific match
 */

module.exports = (query) => new Promise((resolve, reject) => {
  // console.log(query);
  Request.find(query).lean().sort('serviceType').populate('marketSession')
    .exec((err, request) => {
      if (err) {
        reject(err);
      } else {
        resolve(request);
      }
    });
});
