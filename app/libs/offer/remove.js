const Offer = require('../../models/offer');

/**
 * Get a specific match by id
 * @memberOf module:Offer
 * @function get
 * @param {String} plaierServiceId - a valid playerService id
 *
 * @returns {Promise} - Returns a JSON with the deleted objects
 */

module.exports = (playerServiceId) => new Promise((resolve, reject) => {
  Offer.deleteMany({ playerServiceId }).exec((err) => {
    if (err) {
      reject(err);
    } else {
      resolve('Successful deleted');
    }
  });
});
