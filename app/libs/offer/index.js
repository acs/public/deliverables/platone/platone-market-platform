/**
 * @module Offer
 */

const get = require('./get');
const save = require('./save');
const getAll = require('./getAll');
const remove = require('./remove');

const self = {
  get,
  save,
  getAll,
  remove,
};

module.exports = self;
