const Offer = require('../../models/offer');

/**
 * Get all match
 * @memberOf module:Offer
 * @function getAll
 *
 * @returns {Promise} - Returns a JSON with all match
 */

module.exports = () => new Promise((resolve, reject) => {
  Offer.find({}, (err, offers) => {
    if (err) {
      reject(err);
    } else {
      resolve(offers);
    }
  });
});
