const Offer = require('../../models/offer');

/**
 * Save an Request object.
 * @memberOf module:Offer
 * @function save
 * @param {Object} params - Request params
 *
 * @returns {Promise} - Returns a JSON with the saved match
 */

module.exports = (params) => new Promise((resolve, reject) => {
  Offer.findOneAndUpdate({
    playerServiceId: params.playerServiceId, pod: params.pod, index: params.index, powerType: params.powerType, marketSession: params.marketSession,
  },
  params,
  { upsert: true, new: true },
  (err, offer) => {
    if (err) {
      reject(err);
    } else {
      resolve(offer);
    }
  });
});
