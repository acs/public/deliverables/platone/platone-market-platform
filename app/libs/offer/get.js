const Offer = require('../../models/offer');

/**
 * Get a specific match by id
 * @memberOf module:Offer
 * @function get
 * @param {String} id - a valid match id
 *
 * @returns {Promise} - Returns a JSON with the specific match
 */

module.exports = (query) => new Promise((resolve, reject) => {
  // console.log(query);
  Offer.find(query).lean().populate('marketSession').exec((err, offer) => {
    if (err) {
      reject(err);
    } else {
      resolve(offer);
    }
  });
});
