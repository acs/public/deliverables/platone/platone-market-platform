const FlexibilityService = require('../../models/flexibilityService');

/**
 * Save an FlexibilityService object.
 * @memberOf module:FlexibilityService
 * @function save
 * @param {Object} params - FlexibilityService params
 *
 * @returns {Promise} - Returns a JSON with the saved flexibilityService
 */

module.exports = (params) => new Promise((resolve, reject) => {
  FlexibilityService.findOneAndUpdate({ playerServiceId: params.playerServiceId, marketSession: params.marketSession },
    params,
    { setDefaultsOnInsert: true, upsert: true, new: true },
    (err, flexibilityService) => {
      if (err) {
        reject(err);
      } else {
        resolve(flexibilityService);
      }
    });
});
