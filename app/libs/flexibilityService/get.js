const FlexibilityService = require('../../models/flexibilityService');
const MarketSession = require('../../models/marketSession');

/**
 * Get a specific flexibilityService by id
 * @memberOf module:FlexibilityService
 * @function get
 * @param {String} id - a valid flexibilityService id
 *
 * @returns {Promise} - Returns a JSON with the specific flexibilityService
 */

module.exports = (query) => new Promise((resolve, reject) => {
  FlexibilityService.find(query).lean().populate('marketSession').exec((err, flexibilityServices) => {
    if (err) {
      reject(err);
    } else {
      resolve(flexibilityServices);
    }
  });
});
