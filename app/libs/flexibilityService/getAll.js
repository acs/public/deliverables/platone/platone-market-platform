const FlexibilityService = require('../../models/flexibilityService');

/**
 * Get all flexibilityService
 * @memberOf module:FlexibilityService
 * @function getAll
 *
 * @returns {Promise} - Returns a JSON with all flexibilityService
 */

module.exports = () => new Promise((resolve, reject) => {
  FlexibilityService.find({}, (err, flexibilityServices) => {
    if (err) {
      reject(err);
    } else {
      resolve(flexibilityServices);
    }
  });
});
