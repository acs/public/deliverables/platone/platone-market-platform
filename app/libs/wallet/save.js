const Wallet = require('../../models/wallet');

/**
 * Save an Wallet object.
 * @memberOf module:Wallet
 * @function save
 * @param {Object} params - Wallet params
 *
 * @returns {Promise} - Returns a JSON with the saved Wallet
 */

module.exports = (params) => {
  const walletData = new Wallet(params);

  return new Promise((resolve, reject) => {
    Wallet.findOneAndUpdate({ _id: walletData._id },
      walletData,
      { upsert: true, new: false },
      (err, wallet) => {
        if (err) {
          reject(err);
        } else {
          resolve(wallet);
        }
      });
  });
};
