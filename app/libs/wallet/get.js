const Wallet = require('../../models/wallet');

/**
 * Get a specific Wallet by id
 * @memberOf module:Wallet
 * @function get
 * @param {String} id - a valid Wallet id
 *
 * @returns {Promise} - Returns a JSON with the specific Wallet
 */

module.exports = (id) => new Promise((resolve, reject) => {
  Wallet.find({ _id: id }, (err, wallets) => {
    if (err) {
      reject(err);
    } else {
      resolve(wallets);
    }
  });
});
