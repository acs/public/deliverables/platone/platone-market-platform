const Wallet = require('../../models/wallet');

/**
 * Get all Wallet
 * @memberOf module:Wallet
 * @function getAll
 *
 * @returns {Promise} - Returns a JSON with all Wallet
 */

module.exports = () => new Promise((resolve, reject) => {
  Wallet.find({}, (err, wallets) => {
    if (err) {
      reject(err);
    } else {
      resolve(wallets);
    }
  });
});
