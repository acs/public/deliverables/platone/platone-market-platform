const Tso = require('../../models/tso');

/**
 * Save an Tso object.
 * @memberOf module:Tso
 * @function save
 * @param {Object} params - Tso params
 *
 * @returns {Promise} - Returns a JSON with the saved tso
 */

module.exports = (params) => {
  const tsoData = params;

  return new Promise((resolve, reject) => {
    console.log(tsoData);
    Tso.findOneAndUpdate({ pod: tsoData.pod },
      tsoData,
      { new: true },
      (err, tso) => {
        if (err) {
          console.log(err);
          reject(err);
        } else if (!tso) {
          resolve('TSO not found');
        } else {
          resolve(tso);
        }
      });
  });
};
