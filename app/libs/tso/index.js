/**
 * @module Tso
 */

const get = require('./get');
const save = require('./save');
const getAll = require('./getAll');
const remove = require('./remove');
const create = require('./create');
const update = require('./update');

const self = {
  get,
  save,
  getAll,
  remove,
  update,
  create,
};

module.exports = self;
