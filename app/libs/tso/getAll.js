const Tso = require('../../models/tso');

/**
 * Get all Tso
 * @memberOf module:Tso
 * @function getAll
 *
 * @returns {Promise} - Returns a JSON with all tso
 */

module.exports = () => new Promise((resolve, reject) => {
  Tso.find({}, (err, podsTso) => {
    if (err) {
      reject(err);
    } else {
      resolve(podsTso);
    }
  });
});
