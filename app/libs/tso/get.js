const Tso = require('../../models/tso');

/**
 * Get a specific tso by id
 * @memberOf module:Tso
 * @function get
 * @param {String} id - a valid tso id
 *
 * @returns {Promise} - Returns a JSON with the specific tso
 */

module.exports = (query) => new Promise((resolve, reject) => {
  Tso.find(query, (err, podsTso) => {
    if (err) {
      reject(err);
    } else {
      resolve(podsTso);
    }
  });
});
