const Tso = require('../../models/tso');

/**
 * Get a specific tso by id
 * @memberOf module:Tso
 * @function get
 * @param {String} id - a valid tso id
 *
 * @returns {Promise} - Returns a JSON with the specific tso
 */

module.exports = (pod) => new Promise((resolve, reject) => {
  console.log(`TSO to be removed: ${pod}`);
  Tso.remove({ pod }, (err, removedTso) => {
    if (err) {
      console.log(err);
      reject(err);
    } else {
      resolve(removedTso);
    }
  });
});
