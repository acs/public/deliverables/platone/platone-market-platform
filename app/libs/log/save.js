const Log = require('../../models/log');

/**
 * Save an log object.
 * @memberOf module:Log
 * @function save
 * @param {Object} params - Request params
 *
 * @returns {Promise} - Returns a JSON with the saved log
 */

module.exports = (params) => {
  const logData = new Log(params);

  return new Promise((resolve, reject) => {
    Log.findOneAndUpdate(
      { _id: logData._id },
      logData,
      { upsert: true, new: true },
      (err, log) => {
        if (err) {
          reject(err);
        } else {
          resolve(log);
        }
      },
    );
  });
};
