const Log = require('../../models/log');

/**
 * Get all log
 * @memberOf module:Log
 * @function getAll
 *
 * @returns {Promise} - Returns a JSON with all log
 */

module.exports = () => new Promise((resolve, reject) => {
  Log.find({}, (err, logs) => {
    if (err) {
      reject(err);
    } else {
      resolve(logs);
    }
  });
});
