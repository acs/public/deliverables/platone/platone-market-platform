const Log = require('../../models/log');

/**
 * Get a specific log by id
 * @memberOf module:Log
 * @function get
 * @param {String} id - a valid log id
 *
 * @returns {Promise} - Returns a JSON with the specific log
 */

module.exports = (query) => new Promise((resolve, reject) => {
  Log.find(query, (err, log) => {
    if (err) {
      reject(err);
    } else {
      resolve(log);
    }
  });
});
