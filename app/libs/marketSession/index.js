/**
 * @module MarketSession
 */

const get = require('./get');
const save = require('./save');
const getAll = require('./getAll');
const getByTypeAndStatus = require('./getByTypeAndStatus');
const getByType = require('./getByType');
const getExpired = require('./getExpired');
const getSessionClosedByType = require('./getSessionClosedByType');
const getToBeStarted = require('./getToBeStarted');
const getExpiredByActivation = require('./getExpiredByActivation');
const getLastNByType = require('./getLastNByType');

const self = {
  get,
  save,
  getAll,
  getByTypeAndStatus,
  getByType,
  getExpired,
  getSessionClosedByType,
  getToBeStarted,
  getExpiredByActivation,
  getLastNByType,
};

module.exports = self;
