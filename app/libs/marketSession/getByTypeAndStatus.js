const MarketSession = require('../../models/marketSession');

/**
 * Get a specific marketSession by id
 * @memberOf module:MarketSession
 * @function get
 *
 * @returns {Promise} - Returns a JSON with the specific marketSession
 */

module.exports = (marketType) => new Promise((resolve, reject) => {
  MarketSession.findOne({ marketType, status: { $in: ['active', 'created'] } }, (err, marketSessions) => {
    if (err) {
      reject(err);
    } else {
      resolve(marketSessions);
    }
  });
});
