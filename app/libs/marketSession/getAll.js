const MarketSession = require('../../models/marketSession');

/**
 * Get all marketSession
 * @memberOf module:marketSession
 * @function getAll
 *
 * @returns {Promise} - Returns a JSON with all marketSession
 */

module.exports = (params) => new Promise((resolve, reject) => {
  const query = params.query || {};
  MarketSession.find(query, (err, marketSessions) => {
    if (err) {
      reject(err);
    } else {
      resolve(marketSessions);
    }
  });
});
