const MarketSession = require('../../models/marketSession');

/**
 * Get a specific MarketSession by id
 * @memberOf module:MarketSession
 * @function get
 * @param {String} id - a valid MarketSession id
 *
 * @returns {Promise} - Returns a JSON with the specific MarketSession
 */

module.exports = (id) => new Promise((resolve, reject) => {
  // console.log(id);
  MarketSession.findOne({ _id: id }, (err, marketSession) => {
    if (err) {
      reject(err);
    } else {
      resolve(marketSession);
      // console.log(marketSession);
    }
  });
});
