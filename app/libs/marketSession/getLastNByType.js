const MarketSession = require('../../models/marketSession');

/**
 * Get a specific marketSession by id
 * @memberOf module:MarketSession
 * @function get
 *
 * @returns {Promise} - Returns a JSON with the specific marketSession
 */

module.exports = (params) => new Promise((resolve, reject) => {
  MarketSession.find({ marketType: params.marketType, status: params.status }).sort({ _id: -1 }).limit(params.n)
    .exec((err, request) => {
      if (err) {
        reject(err);
      } else {
        resolve(request);
      }
    });
});
