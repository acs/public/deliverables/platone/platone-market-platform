const MarketSession = require('../../models/marketSession');

/**
 * Get a specific marketSession by id
 * @memberOf module:MarketSession
 * @function get
 *
 * @returns {Promise} - Returns a JSON with the specific marketSession
 */

module.exports = () => new Promise((resolve, reject) => {
  MarketSession.findOne({ status: 'created', start: { $lt: Date.now() } }, (err, marketSessions) => {
    if (err) {
      reject(err);
    } else {
      resolve(marketSessions);
    }
  });
});
