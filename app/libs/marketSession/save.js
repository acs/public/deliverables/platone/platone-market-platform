const MarketSession = require('../../models/marketSession');

/**
 * Save an MarketSession object.
 * @memberOf module:MarketSession
 * @function save
 * @param {Object} params - MarketSession params
 *
 * @returns {Promise} - Returns a JSON with the saved MarketSession
 */

module.exports = (params) => {
  const marketSessionData = new MarketSession(params);
  // TODO remove fixed participants
  marketSessionData.participants = [{ playerId: 'ACEAE', playerType: 'aggregator' }, { playerId: 'DSO', playerType: 'DSO' }];
  return new Promise((resolve, reject) => {
    MarketSession.findOneAndUpdate({ _id: marketSessionData._id },
      marketSessionData,
      { upsert: true, new: true },
      (err, marketSession) => {
        if (err) {
          reject(err);
        } else {
          resolve(marketSession);
        }
      });
  });
};
