const Settlement = require('../../models/settlement');

/**
 * Get last settlement
 * @memberOf module:Settlement
 * @function getAll
 *
 * @returns {Promise} - Returns a JSON with all settlement
 */

module.exports = () => new Promise((resolve, reject) => {
  Settlement.findOne({ sent: false }).populate('marketSession')
    .exec((err, settlement) => {
      if (err) {
        reject(err);
      } else {
        resolve(settlement);
      }
    });
});
