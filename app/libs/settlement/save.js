const Settlement = require('../../models/settlement');

/**
 * Save an Settlement object.
 * @memberOf module:Settlement
 * @function save
 * @param {Object} params - Settlement params
 *
 * @returns {Promise} - Returns a JSON with the saved settlement
 */

module.exports = (params) => {
  const settlementData = new Settlement(params);

  return new Promise((resolve, reject) => {
    Settlement.findOneAndUpdate({ _id: settlementData._id },
      settlementData,
      { upsert: true, new: false },
      (err, settlement) => {
        if (err) {
          reject(err);
        } else {
          resolve(settlement);
        }
      });
  });
};
