const Settlement = require('../../models/settlement');

/**
 * Get all settlement
 * @memberOf module:Settlement
 * @function getAll
 *
 * @returns {Promise} - Returns a JSON with all settlement
 */

module.exports = () => new Promise((resolve, reject) => {
  Settlement.find({}, (err, settlement) => {
    if (err) {
      reject(err);
    } else {
      resolve(settlement);
    }
  });
});
