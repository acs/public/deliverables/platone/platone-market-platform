/**
 * @module Settlement
 */

const get = require('./get');
const save = require('./save');
const getAll = require('./getAll');
const getUnsent = require('./getUnsent');

const self = {
  get,
  save,
  getAll,
  getUnsent,
};

module.exports = self;
