const Settlement = require('../../models/settlement');

/**
 * Get a specific settlement by id
 * @memberOf module:Settlement
 * @function get
 * @param {String} id - a valid settlement id
 *
 * @returns {Promise} - Returns a JSON with the specific Settlement
 */

module.exports = (query) => new Promise((resolve, reject) => {
  Settlement.find(query, (err, settlement) => {
    if (err) {
      reject(err);
    } else {
      resolve(settlement);
    }
  });
});
