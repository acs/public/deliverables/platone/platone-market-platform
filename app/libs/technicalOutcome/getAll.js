const TechnicalOutcome = require('../../models/technicalOutcome');

/**
 * Get all technicalOutcome
 * @memberOf module:TechnicalOutcome
 * @function getAll
 *
 * @returns {Promise} - Returns a JSON with all technicalOutcome
 */

module.exports = () => new Promise((resolve, reject) => {
  TechnicalOutcome.find({}, (err, technicalOutcomes) => {
    if (err) {
      reject(err);
    } else {
      resolve(technicalOutcomes);
    }
  });
});
