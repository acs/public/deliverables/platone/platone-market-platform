const TechnicalOutcome = require('../../models/technicalOutcome');

/**
 * Get a specific technicalOutcome by id
 * @memberOf module:TechnicalOutcome
 * @function get
 * @param {String} id - a valid technicalOutcome id
 *
 * @returns {Promise} - Returns a JSON with the specific technicalOutcome
 */

module.exports = (query) => new Promise((resolve, reject) => {
  TechnicalOutcome.find(query, (err, technicalOutcomes) => {
    if (err) {
      reject(err);
    } else {
      resolve(technicalOutcomes);
    }
  });
});
