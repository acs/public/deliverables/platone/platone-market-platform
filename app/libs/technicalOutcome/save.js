const TechnicalOutcome = require('../../models/technicalOutcome');

/**
 * Save an technicalOutcome object.
 * @memberOf module:TechnicalOutcome
 * @function save
 * @param {Object} params - technicalOutcome params
 *
 * @returns {Promise} - Returns a JSON with the saved technicalOutcome
 */

module.exports = (params) => {
  const technicalOutcomeData = new TechnicalOutcome(params);

  return new Promise((resolve, reject) => {
    TechnicalOutcome.findOneAndUpdate({ _id: technicalOutcomeData._id },
      technicalOutcomeData,
      { upsert: true, new: true },
      (err, technicalOutcome) => {
        if (err) {
          reject(err);
        } else {
          resolve(technicalOutcome);
        }
      });
  });
};
