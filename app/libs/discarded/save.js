const Discarded = require('../../models/discarded');

/**
 * Save a Discarded object.
 * @memberOf module:Discarded
 * @function save
 * @param {Object} params - Request params
 *
 * @returns {Promise} - Returns a JSON with the saved discarded object
 */

module.exports = (params) => new Promise((resolve, reject) => {
  Discarded.findOneAndUpdate({
    _id: params._id,
  },
  params,
  { upsert: true, new: true },
  (err, discarded) => {
    if (err) {
      reject(err);
    } else {
      resolve(discarded);
    }
  });
});
