const Discarded = require('../../models/discarded');

/**
 * Get a specific discarded by id
 * @memberOf module:Discarded
 * @function get
 * @param {String} id - a valid match id
 *
 * @returns {Promise} - Returns a JSON with the specific discarded object
 */

module.exports = (query) => new Promise((resolve, reject) => {
  console.log(query);
  Discarded.find(query).lean().populate('marketSession').exec((err, discarded) => {
    if (err) {
      reject(err);
    } else {
      resolve(discarded);
    }
  });
});
