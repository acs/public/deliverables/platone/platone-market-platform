const Discarded = require('../../models/discarded');

/**
 * Get all match
 * @memberOf module:Discarded
 * @function getAll
 *
 * @returns {Promise} - Returns a JSON with all match
 */

module.exports = () => new Promise((resolve, reject) => {
  Discarded.find({}, (err, discarded) => {
    if (err) {
      reject(err);
    } else {
      resolve(discarded);
    }
  });
});
