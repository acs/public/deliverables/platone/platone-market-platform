/**
 * @module Discarded
 */

const get = require('./get');
const save = require('./save');
const getAll = require('./getAll');
const setValidated = require('./setValidated');

const self = {
  get,
  save,
  getAll,
  setValidated,
};

module.exports = self;
