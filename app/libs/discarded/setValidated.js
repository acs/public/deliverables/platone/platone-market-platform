const Discarded = require('../../models/discarded');

/**
 * Get a specific discarded by id
 * @memberOf module:Discarded
 * @function get
 * @param {String} id - a valid match id
 *
 * @returns {Promise} - Returns a JSON with the specific discarded object
 */

module.exports = (query) => new Promise((resolve, reject) => {
  console.log(query);
  Discarded.findOneAndUpdate(query, { $set: { validated: true } }).populate('marketSession').exec((err, discarded) => {
    console.log(discarded);
    if (err) {
      reject(err);
    } else {
      resolve(discarded);
    }
  });
});
