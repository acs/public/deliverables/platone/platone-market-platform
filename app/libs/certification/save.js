const Certification = require('../../models/certification');

/**
 * Save an Certification object.
 * @memberOf module:Certification
 * @function save
 * @param {Object} params - Certification params
 *
 * @returns {Promise} - Returns a JSON with the saved certification
 */

module.exports = (params) => {
  const certificationData = new Certification(params);

  return new Promise((resolve, reject) => {
    Certification.findOneAndUpdate({ _id: certificationData._id },
      certificationData,
      { upsert: true, new: false },
      (err, certification) => {
        if (err) {
          reject(err);
        } else {
          resolve(certification);
        }
      });
  });
};
