const Certification = require('../../models/certification');

/**
 * Get a specific customer by id
 * @memberOf module:Certification
 * @function get
 * @param {String} id - a valid customer id
 *
 * @returns {Promise} - Returns a JSON with the specific certifications
 */

module.exports = (query) => new Promise((resolve, reject) => {
  Certification.find(query, (err, certifications) => {
    if (err) {
      reject(err);
    } else {
      resolve(certifications);
    }
  });
});
