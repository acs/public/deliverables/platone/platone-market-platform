const SmartContract = require('../../models/smartContract');

/**
 * Save an SmartContract object.
 * @memberOf module:SmartContract
 * @function save
 * @param {Object} params - MeasSmartContracturements params
 *
 * @returns {Promise} - Returns a JSON with the saved smartContract
 */

module.exports = (params) => {
  const smartContractData = new SmartContract(params);

  return new Promise((resolve, reject) => {
    SmartContract.findOneAndUpdate({ _id: smartContractData._id },
      smartContractData,
      { upsert: true, new: true },
      (err, smartContract) => {
        if (err) {
          reject(err);
        } else {
          resolve(smartContract);
        }
      });
  });
};
