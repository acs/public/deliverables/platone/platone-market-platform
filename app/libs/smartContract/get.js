const SmartContract = require('../../models/smartContract');

/**
 * Get a specific smartContract by id
 * @memberOf module:SmartContract
 * @function get
 * @param {String} id - a valid smartContract id
 *
 * @returns {Promise} - Returns a JSON with the specific SmartContract
 */

module.exports = (query) => new Promise((resolve, reject) => {
  SmartContract.find(query, (err, smartContract) => {
    if (err) {
      reject(err);
    } else {
      resolve(smartContract);
    }
  });
});
