const SmartContract = require('../../models/smartContract');

/**
 * Get all smartContract
 * @memberOf module:SmartContract
 * @function getAll
 *
 * @returns {Promise} - Returns a JSON with all smartContract
 */

module.exports = () => new Promise((resolve, reject) => {
  SmartContract.find({}, (err, smartContract) => {
    if (err) {
      reject(err);
    } else {
      resolve(smartContract);
    }
  });
});
