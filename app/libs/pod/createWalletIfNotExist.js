const Pod = require('../../models/pod');
const ContractUtil = require('../../services/contracts').utils();

/**
 * Save an Pod object.
 * @memberOf module:Pod
 * @function save
 * @param {Object} params - Pod params
 *
 * @returns {Promise} - Returns a JSON with the saved pod
 */

module.exports = (params) => new Promise((resolve, reject) => {
  Pod.findOne({ pod: params.podId }).then((result) => {
    if (result == null) {
      console.error('Pod does not exist');
      reject(new Error('Pod does not exist'));
    } else {
      const pod = result;
      console.log(pod);
      const address = pod.wallet;
      if (address) {
        console.log('Wallet address is not null');
        resolve('Wallet address is not null');
      } else {
        console.log('CREATE WALLET ADDRESS');
        ContractUtil.createWallet().then((addressWallet) => {
          console.log(addressWallet);
          ContractUtil.unlock(addressWallet);
          pod.wallet = addressWallet;
          Pod.findOneAndUpdate({ pod: pod.pod }, pod, { new: true },
            (err, updatedPod) => {
              if (err) {
                console.log(err);
                reject(err);
              } else {
                resolve(updatedPod);
              }
            });
        }).catch((err) => {
          console.log(err);
          reject(err);
        });
      }
    }
  }).catch((err) => {
    console.log(err);
    reject(err);
  });
});
