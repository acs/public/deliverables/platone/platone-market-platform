const Pod = require('../../models/pod');

/**
 * Get a specific pod by id
 * @memberOf module:Pod
 * @function get
 * @param {String} id - a valid pod id
 *
 * @returns {Promise} - Returns a JSON with the specific pod
 */

module.exports = (query) => new Promise((resolve, reject) => {
  Pod.find(query, (err, pods) => {
    if (err) {
      reject(err);
    } else {
      resolve(pods);
    }
  });
});
