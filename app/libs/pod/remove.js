const Pod = require('../../models/pod');

/**
 * Get a specific pod by id
 * @memberOf module:Pod
 * @function get
 * @param {String} id - a valid pod id
 *
 * @returns {Promise} - Returns a JSON with the specific pod
 */

module.exports = (pod) => new Promise((resolve, reject) => {
  console.log(`POD to be removed: ${pod}`);
  Pod.remove({ pod }, (err, removedPod) => {
    if (err) {
      console.log(err);
      reject(err);
    } else {
      resolve(removedPod);
    }
  });
});
