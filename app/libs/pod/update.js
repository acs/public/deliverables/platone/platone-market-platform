const Pod = require('../../models/pod');

/**
 * Save an Pod object.
 * @memberOf module:Pod
 * @function save
 * @param {Object} params - Pod params
 *
 * @returns {Promise} - Returns a JSON with the saved pod
 */

module.exports = (params) => {
  const podData = params;

  return new Promise((resolve, reject) => {
    console.log(podData);
    Pod.findOneAndUpdate({ pod: podData.pod },
      podData,
      { new: true },
      (err, pod) => {
        if (err) {
          console.log(err);
          reject(err);
        } else if (!pod) {
          resolve('POD not found');
        } else {
          resolve(pod);
        }
      });
  });
};
