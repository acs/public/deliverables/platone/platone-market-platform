const Pod = require('../../models/pod');

/**
 * Get all pod
 * @memberOf module:Pod
 * @function getAll
 *
 * @returns {Promise} - Returns a JSON with all pod
 */

module.exports = () => new Promise((resolve, reject) => {
  Pod.find({}, (err, pods) => {
    if (err) {
      reject(err);
    } else {
      resolve(pods);
    }
  });
});
