/**
 * @module Pod
 */

const get = require('./get');
const save = require('./save');
const getAll = require('./getAll');
const remove = require('./remove');
const create = require('./create');
const createWalletIfNotExist = require('./createWalletIfNotExist');
const update = require('./update');

const self = {
  get,
  save,
  getAll,
  remove,
  update,
  create,
  createWalletIfNotExist,
};

module.exports = self;
