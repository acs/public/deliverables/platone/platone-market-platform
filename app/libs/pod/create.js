const Pod = require('../../models/pod');
const ContractUtil = require('../../services/contracts').utils();

/**
 * Save an Pod object.
 * @memberOf module:Pod
 * @function save
 * @param {Object} params - Pod params
 *
 * @returns {Promise} - Returns a JSON with the saved pod
 */

module.exports = (params) => {
  const podData = params;

  return new Promise((resolve, reject) => {
    console.log('NUOVO');
    console.log('CREATE POD');
    ContractUtil.createWallet().then((address) => {
      console.log(address);
      ContractUtil.unlock(address);
      podData.wallet = address;
      Pod.create(podData,
        (err, pod) => {
          if (err) {
            console.log(err);
            reject(err);
          } else {
            resolve(pod);
          }
        });
    }).catch((err) => {
      console.log(err);
    });
  });
};
