pragma solidity ^0.5.16;
pragma experimental ABIEncoderV2;

contract PlatoneSettlement {

    struct Timeframe {
        uint256[] intervals;
        uint256[] prices;
        string[] types;
    }

    mapping(string => Timeframe) settlement;
    address public owner;

    constructor(Timeframe memory day1, Timeframe memory day2,  Timeframe memory day3, Timeframe memory day4, Timeframe memory day5, Timeframe memory day6, Timeframe memory day7) public {
        settlement['mon'] = day1;
        settlement['tue'] = day2;
        settlement['wed'] = day3;
        settlement['thu'] = day4;
        settlement['fri'] = day5;
        settlement['sat'] = day6;
        settlement['sun'] = day7;
        owner = msg.sender;
    }

    modifier onlyOwner() {
        require(msg.sender == owner, "Only owner authorized ");
        _;
    }

    function getSettlement()
        public
        view
        returns (Timeframe memory mon, Timeframe memory tue, Timeframe memory wed, Timeframe memory thu, Timeframe memory fri, Timeframe memory sat, Timeframe memory sun)
    {
        return (
            settlement['mon'],
            settlement['tue'],
            settlement['wed'],
            settlement['thu'],
            settlement['fri'],
            settlement['sat'],
            settlement['sun']
        );
    }
}
