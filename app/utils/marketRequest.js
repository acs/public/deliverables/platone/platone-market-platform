const request = require('retry-request', {
  // eslint-disable-next-line global-require
  request: require('request'),
});
const moment = require('moment');
const config = require('../config/config');
const externalTokenLib = require('../libs/externalToken');

function getBasicAuth(username, password) {
  const string = `${username}:${password}`;
  const base64String = Buffer.from(string).toString('base64');

  return `Basic ${base64String}`;
}

// factory
function requestClient() {
  // private
  const options = {
    url: `https://${config.marketoutcome.host}/`,
    rejectUnauthorized: false,
    strictSSL: false,
  };

  // public
  const self = {
    getOptions() {
      return Object.create(options);
    },
    getToken() {
      return new Promise((resolve, reject) => {
        // DB
        externalTokenLib.getNotExpired(config.marketoutcome.clientID)
          .then((token) => {
            if (token.accessToken) {
              const tokenStr = `Bearer ${token.accessToken}`;
              resolve(tokenStr);
            } else {
              // token expired, request a new one from the market platform
              const tokenOptions = {
                url: config.marketoutcome.tokenURL,
                headers: {
                  'User-Agent': 'ENG request client',
                  Authorization: getBasicAuth(config.marketoutcome.clientID, config.marketoutcome.clientSecret),
                  'Content-Type': 'application/x-www-form-urlencoded',
                },
                method: 'POST',
                form: {
                  grant_type: 'password', scope: 'openid', username: config.marketoutcome.username, password: config.marketoutcome.password,
                },
                rejectUnauthorized: false,
                strictSSL: false,
              };
              request(tokenOptions, (error, response, body) => {
                if (error) {
                  reject(error);
                } else if (response.statusCode >= 400) {
                  reject(body || response);
                } else {
                  console.log('GET TOKEN');
                  console.log(body);
                  const bodyJson = JSON.parse(body);
                  const now = moment().utc();
                  const expire = now.add(Number(bodyJson.expires_in - 300), 'seconds');
                  // update in db
                  externalTokenLib.update(config.marketoutcome.clientID, bodyJson.access_token, expire)
                    .then(() => {
                      resolve(`Bearer ${bodyJson.access_token}`);
                    })
                    .catch((err) => reject(err));
                }
              });
            }
          })
          .catch((error) => reject(error));
      });
    },
    post(service, body) {
      return new Promise((resolve, reject) => {
        const opts = this.getOptions();
        opts.method = 'POST';
        opts.json = true;
        opts.url += service;
        opts.body = body;
        opts.headers = {};
        opts.headers['Content-Type'] = 'application/json';

        this.getToken()
          .then((tokenStr) => {
            console.log('Token from DSOTP received');
            opts.headers.Authorization = tokenStr;
            request(opts, (error, response, bodyResponse) => {
              if (error) {
                console.log(error);
                reject(error);
              } else if (response.statusCode >= 400) {
                console.log(bodyResponse);
                reject(bodyResponse || response);
              } else {
                console.log('Technical Outcome sent');
                console.log(bodyResponse);
                resolve(bodyResponse);
              }
            });
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    get(service) {
      return new Promise((resolve, reject) => {
        const opts = this.getOptions();
        opts.method = 'GET';
        opts.url += service;
        opts.headers['Content-Type'] = 'application/json';

        this.getToken()
          .then((tokenStr) => {
            opts.headers.Authorization = tokenStr;
            request(opts, (error, response, body) => {
              if (error) {
                reject(error);
              } else if (response.statusCode >= 400) {
                reject(body || response);
              } else {
                const bodyJson = JSON.parse(body);
                resolve(bodyJson);
              }
            });
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
  };
  return Object.create(self);
}
module.exports = (() => requestClient());
