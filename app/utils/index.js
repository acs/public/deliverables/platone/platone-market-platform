const MarketRequest = require('./marketRequest');
const TSORequest = require('./tsoRequest');

const self = {
  MarketRequest,
  TSORequest,
};

module.exports = self;
