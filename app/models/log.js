const mongoose = require('mongoose');

const { Schema } = mongoose;

const logSchema = Schema({
  type: {
    type: String,
    default: "message_received",
    enum: [
      "message_received",
      "message_sent",
      "start_execution",
      "end_execution",
    ],
  },
  flow: {
    type: String,
    default: "flow 0",
    enum: [
      "flow0",
      "flow1",
      "flow2",
      "flow3",
      "flow4",
      "flow5",
      "flow6",
      "flow7",
      "flow8",
    ],
  },
  platform: {
    type: String,
    default: "MP",
    enum: ["AP", "DSOTP", "MP", "SCD", "BAL", "TSO", "LN"],
  },
  referenceId: { type: String },
  dateTime: { type: Date },
});

const Log = mongoose.model('Log', logSchema);
module.exports = Log;
