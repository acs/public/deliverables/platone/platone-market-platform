const mongoose = require('mongoose');

const { Schema } = mongoose;

const marketSessionSchema = Schema({
  marketType: {
    type: String, default: 'dayAhead', enum: ['dayAhead', 'realTime'], required: true,
  },
  status: {
    type: String, default: 'created', enum: ['created', 'active', 'closed', 'settled', 'empty'], required: true,
  },
  linkedSession: { type: Schema.Types.ObjectId, ref: 'MarketSession' },
  duration: { type: Number },
  interval: { type: Number },
  start: { type: Date },
  end: { type: Date },
  startActivation: { type: Date },
  endActivation: { type: Date },
  participants: [],
});

const MarketSession = mongoose.model('MarketSession', marketSessionSchema);
module.exports = MarketSession;
