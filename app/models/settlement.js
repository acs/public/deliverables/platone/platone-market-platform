const mongoose = require('mongoose');

const { Schema } = mongoose;

const settlementSchema = Schema({
  startTime: { type: Date },
  marketSession: {
    type: Schema.Types.ObjectId, ref: 'MarketSession', required: true, unique: true,
  },
  marketOutcome: {
    type: Schema.Types.ObjectId, ref: 'MarketOutcome', required: true, unique: true,
  },
  marketType: { type: String, default: 'dayAhead', enum: ['dayAhead', 'realTime'] },
  flexibility: [{
    pod: { type: String },
    power: [{
      index: { type: String }, // 0 to 15 or from 0 95
      requestedP: { type: Number },
      measuredP: { type: Number },
      paidP: { type: Number },
      tokenP: { type: Number },
      requestedQ: { type: Number },
      measuredQ: { type: Number },
      paidQ: { type: Number },
      tokenQ: { type: Number },
      requestPlayerId: { type: String },
      offerPlayerId: { type: String },
    }],
  }],
  sent: { type: Boolean },
});

const Settlement = mongoose.model('Settlment', settlementSchema);
module.exports = Settlement;
