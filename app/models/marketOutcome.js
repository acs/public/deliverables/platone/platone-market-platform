const mongoose = require('mongoose');

const { Schema } = mongoose;

const marketOutcomeSchema = Schema({

  startTime: { type: Date },
  duration: { type: Number }, /* possible value 4h or 24h */
  interval: { type: Number }, /* 15 minutes */
  marketType: {
    type: String, default: 'dayAhead', enum: ['dayAhead', 'realTime'], required: true,
  },
  marketSession: {
    type: Schema.Types.ObjectId, ref: 'marketSession', required: true, unique: true,
  },
  // playerServiceId: { type: String },
  flexibility: [{
    pod: { type: String },
    power: [{
      index: { type: String }, // 0 to 15 or from 0 95
      p: { type: Number },
      priorityP: { type: Number },
      q: { type: Number },
      priorityQ: { type: Number },
    }],
  }],
});

const MarketOutcome = mongoose.model('MarketOutcome', marketOutcomeSchema);
module.exports = MarketOutcome;
