const mongoose = require('mongoose');

const { Schema } = mongoose;

const smartContractSchema = Schema({
  address: { type: String },
  owner: { type: String },
});

const SmartContract = mongoose.model('SmartContract', smartContractSchema);
module.exports = SmartContract;
