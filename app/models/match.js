const mongoose = require('mongoose');

const { Schema } = mongoose;

const matchSchema = Schema({
  playerId: { type: String }, // DSO/TSO ID
  marketSession: { type: Schema.Types.ObjectId, ref: 'MarketSession' },
  index: { type: String }, // 0 to 15 or from 0 95
  powerType: { type: String, default: 'active', enum: ['active', 'reactive'] },
  powerRequested: { type: Number },
  priceRequested: { type: Number },
  acceptedPrice: { type: Number },
  acceptedOffers: [],
});

const Request = mongoose.model('Match', matchSchema);
module.exports = Request;
