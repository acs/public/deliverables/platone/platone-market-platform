const mongoose = require('mongoose');

const { Schema } = mongoose;

const clientSchema = Schema({
  id: String,
  clientId: { type: String, unique: true },
  clientSecret: String,
  grants: [String],
  redirectUris: [String],
});

const Client = mongoose.model('Client', clientSchema);
module.exports = Client;
