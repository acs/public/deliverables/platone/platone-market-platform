const ClientModel = require('./oauthClient');
const TokenModel = require('./oauthToken');
const UserModel = require('./user');

/*
 * Methods used by all grant types.
 */

const getAccessToken = (accessToken, callback) => {
  TokenModel.findOne({ accessToken }, {}, { lean: true }, (err, tokenDB) => {
    if (!tokenDB) {
      callback('Token not found');
    }

    callback(err, tokenDB);
  });
};

const getClient = (clientId, clientSecret, callback) => {
  ClientModel.findOne({ clientId, clientSecret }, {}, { lean: true }, (err, client) => {
    if (!client) {
      callback('Client not found');
    }

    callback(err, client);
  });
};

const saveToken = (token, client, user, callback) => {
  const newToken = token;
  newToken.client = {
    id: client.clientId,
  };

  newToken.user = {
    username: user.username,
  };

  const tokenInstance = new TokenModel(newToken);
  tokenInstance.save((err, tokenDB) => {
    if (!tokenDB) {
      callback('Token not saved');
    }

    callback(err, tokenDB);
  });
};

/*
 * Method used only by password grant type.
 */

const getUser = (username, password, callback) => {
  UserModel.findOne({ username, password }, {}, { lean: true }, (err, user) => {
    if (!user) {
      callback('User not found');
    }

    callback(err, user);
  });
};

/*
 * Method used only by client_credentials grant type.
 */

const getUserFromClient = (client, callback) => {
  ClientModel.findOne({ clientId: client.clientId, clientSecret: client.clientSecret, grants: 'client_credentials' },
    {}, { lean: true }, (err, clientDB) => {
      if (!clientDB) {
        callback('Client not found');
      }
      callback(err, { username: '' });
    });
};

/*
 * Methods used only by refresh_token grant type.
 */

const getRefreshToken = (refreshToken, callback) => {
  TokenModel.findOne({ refreshToken }, {}, { lean: true }, (err, token) => {
    if (!token) {
      callback('Token not found');
    }
    callback(err, token);
  });
};

const revokeToken = (token, callback) => {
  TokenModel.deleteOne({ refreshToken: token.refreshToken }, (err, result) => {
    const deleteSuccess = result && result.deletedCount === 1;

    if (!deleteSuccess) {
      callback('Token not deleted');
    }

    callback(err, deleteSuccess);
  });
};

module.exports = {
  getAccessToken,
  getClient,
  saveToken,
  getUser,
  getUserFromClient,
  getRefreshToken,
  revokeToken,
};
