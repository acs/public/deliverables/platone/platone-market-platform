const mongoose = require('mongoose');

const { Schema } = mongoose;

const externalTokenSchema = Schema({
  accessToken: String,
  accessTokenExpiresAt: Date,
  clientId: String,
});

const ExternalToken = mongoose.model('ExternalToken', externalTokenSchema);
module.exports = ExternalToken;
