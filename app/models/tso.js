const mongoose = require('mongoose');

const { Schema } = mongoose;

const tsoSchema = Schema({
  pod: { type: String, required: true, unique: true },
  aggregatorId: { type: String, required: true },
  zone: { type: String },
  pomId: { type: String },
  pnp: { type: Number }, /* Potenza Attiva nominale prelievo */
  inp: { type: Number }, /* <PotenzaImmissione-KW> Potenza Attiva nominale immissione */
  inq: { type: Number }, /* <ReattivaNominaleInduttiva_kVAr> /*Potenza reattiva nominale Induttiva */
  cnq: { type: Number }, /* <ReattivaNominakeCapacitiva_kVAr> /*Potenza reattiva nominale Capacitiva */
  cosPhi: { type: Object }, /* Coseno dell’angolo di potenza (valore alfanumerio tra 0 e 1 e A/R) */
  sendTime: { type: Date }, /* 2020-07-24T10:00:00+2, -- format ISO-8601: YYYY-MM-DDThh:mm:ss[.mmm]TZD Timestamp invio dati */
  validityStart: { type: Date }, /* 2020-07-24T10:00:00+2, -- format ISO-8601: YYYY-MM-DDThh:mm:ss[.mmm]TZD /*Timestamp decorrenza validazione dati */
  flexibilityType: { type: String, default: 'continuous', enum: ['continuous', 'discrete'] }, /* continous|discrete /*Tipo servizio (se continuo o discreto) */
  smartContractId: { type: Schema.Types.ObjectId, ref: 'SmartContract' },
  powerCurvesDuration: { type: Number }, /* 24h, possible value 24h */
  powerCurveInterval: { type: Number }, /* 15m, durata del singolo intervallo es 15 minuti */
  powerBaselineCurves: {
    workday: [], /* [{index,p,q},...{index,p,q}], */
    preholiday: [], /* [{index,p,q},...{index,p,q}], */
    holiday: [], /* [{index,p,q},...{index,p,q}] */
  }, /* Curve tipiche della baseline di potenza (P in kW,Q in kVAr) per il POD */
  maxFlexibility: {
    upperP: { type: Number }, /* Maximum flexibility Active power to rise [kW] */
    downP: { type: Number }, /* Flessibilità massima Potenza attiva a scendere [kW] */
    upperQ: { type: Number }, /* Flessibilità massima Potenza reattiva a salire [kVAr] */
    downQ: { type: Number }, /* Flessibilità massima Potenza reattiva a scendere [kVAr] */
  },
});

const Tso = mongoose.model('Tso', tsoSchema);
module.exports = Tso;
