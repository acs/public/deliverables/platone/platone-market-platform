const mongoose = require('mongoose');

const { Schema } = mongoose;

const flexibilityServiceSchema = Schema({
  createdAt: { type: Date, default: Date.now },
  playerId: { type: String, required: true }, // aggregatorId
  startTime: { type: Date },
  duration: { type: Number }, /* possible value 4h or 24h */
  interval: { type: Number }, /* 15 minutes */
  serviceType: { type: String, default: 'offer', enum: ['offer', 'DSO_request', 'TSO_request'] },
  marketType: {
    type: String, default: 'dayAhead', enum: ['dayAhead', 'realTime'], required: true,
  },
  marketSession: { type: Schema.Types.ObjectId, ref: 'MarketSession' },
  playerServiceId: { type: String, required: true },
  flexibility: [{
    pod: { type: Object, required: true },
    realTime: { type: Boolean }, // replicable in realTime market
    flexibleBlockFlag: { type: Boolean },
    flexibleBlock: {
      availableTimeSlots: [{ type: Boolean }],
      maxTimeSlots: { type: Number },
      pMax: { type: Number },
      pPrice: { type: Number },
      minTimeSlotGroup: { type: Number },
    },
    power: [{
      index: { type: String }, // 0 to 15 or from 0 95
      p: { type: Number },
      pPrice: { type: Number },
      q: { type: Number },
      qPrice: { type: Number },
    }],
  }],
});

const FlexibilityService = mongoose.model('FlexibilityService', flexibilityServiceSchema);
module.exports = FlexibilityService;
