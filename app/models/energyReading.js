const mongoose = require('mongoose');

const { Schema } = mongoose;

const energyReadingSchema = Schema({
  podId: { type: String, required: true, unique: true },
  aggregatorId: { type: String, required: true, unique: true },
  dateTime: { type: Date },
  anomaly: { type: String },
  measures: {
    energy: {
      absorbedActiveEnergy: {
        value: { type: Number },
        quality: { type: Number },
      },
      injectedActiveEnergy: {
        value: { type: Number },
        quality: { type: Number },
      },
      absorbedInductiveReactiveEnergy: {
        value: { type: Number },
        quality: { type: Number },
      },
      injectedInductiveReactiveEnergy: {
        value: { type: Number },
        quality: { type: Number },
      },
      absorbedCapacitiveReactiveEnergy: {
        value: { type: Number },
        quality: { type: Number },
      },
      injectedCapacitiveReactiveEnergy: {
        value: { type: Number },
        quality: { type: Number },
      },
    },
    power: {
      activePower: { type: Number },
      reactivePower: { type: Number },
    },
  },
  setPoint: {
    marketOutcomeId: { type: String }, // ID Offer
    activePower: { type: Number },
    reactivePower: { type: Number },
  },
});

const EnergyReading = mongoose.model('EnergyReading', energyReadingSchema);
module.exports = EnergyReading;
