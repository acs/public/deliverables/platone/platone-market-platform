const mongoose = require('mongoose');

const { Schema } = mongoose;

const validateOutcomeSchema = Schema({
  startTime: { type: Date },
  duration: { type: Number }, /* possible value 4h or 24h */
  interval: { type: Number }, /* 15 minutes */
  marketType: { type: String, default: 'dayAhead', enum: ['dayAhead', 'realTime'] },
  marketSession: {
    type: Schema.Types.ObjectId, ref: 'MarketSession', required: true, unique: true,
  },
  marketOutcome: {
    type: Schema.Types.ObjectId, ref: 'MarketOutcome', required: true, unique: true,
  },
  flexibility: [{
    pod: { type: String },
    power: [{
      index: { type: String }, // 0 to 15 or from 0 95
      acceptedPValue: { type: Number },
      acceptedQValue: { type: Number },
      acceptedPPrice: { type: Number },
      acceptedQPrice: { type: Number },
      acceptedPtype: { type: String }, // Visualization
      acceptedPMarket: { type: Number }, // Visualization
      pOfferd: { type: Number }, // Visualization
      rejectionTypeP: { type: String }, // status of the validation
      rejectionTypeQ: { type: String }, // status of the validation
      playerServiceId: { type: String },
      playerRequestId: { type: String }, // internal field for checking validation
      playerOfferId: { type: String }, // internal field for checking validation
    }],
  }],
});

const ValidateOutcome = mongoose.model('ValidateOutcome', validateOutcomeSchema);
module.exports = ValidateOutcome;
