const mongoose = require('mongoose');

const { Schema } = mongoose;

const technicalOutcomeSchema = Schema({
  startTime: { type: Date },
  duration: { type: Number }, /* possible value 4h or 24h */
  interval: { type: Number }, /* 15 minutes */
  marketType: { type: String, default: 'dayAhead', enum: ['dayAhead', 'realTime'] },
  marketSession: {
    type: Schema.Types.ObjectId, ref: 'marketSession', required: true, unique: true,
  },
  marketOutcome: {
    type: Schema.Types.ObjectId, ref: 'marketOutcome', required: true, unique: true,
  },
  flexibility: [{
    pod: { type: String },
    power: [{
      index: { type: Number }, // 0 to 15 or from 0 95
      acceptedPValue: { type: Number },
      acceptedQValue: { type: Number },
      acceptedP: { type: String }, /* If partially accepted: OK, if rejected: KO and acceptedPvalue: null */
      acceptedQ: { type: String }, /* If partially accepted: OK, if rejected: KO and acceptedPvalue: null */
    }],
  }],
});

const TechnicalOutcome = mongoose.model('TechnicalOutcome', technicalOutcomeSchema);
module.exports = TechnicalOutcome;
