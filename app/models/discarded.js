const mongoose = require('mongoose');

const { Schema } = mongoose;

const discardedSchema = Schema({
  playerId: { type: String }, // Aggregator ID
  playerServiceId: { type: String }, // Aggregator ServiceId
  marketSession: { type: Schema.Types.ObjectId, ref: 'MarketSession' },
  index: { type: Number }, // 0 to 15 or from 0 95
  realTime: { type: Boolean },
  powerType: { type: String, default: 'active', enum: ['active', 'reactive'] },
  pod: { type: String },
  power: { type: Number },
  price: { type: Number },
  validated: { type: Boolean, default: false },
});

const Request = mongoose.model('Discarded', discardedSchema);
module.exports = Request;
