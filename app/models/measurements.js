const mongoose = require('mongoose');

const { Schema } = mongoose;

const measurementsSchema = Schema({
  aggregatorId: { type: String },
  pod: { type: String, required: true },
  dataTime: { type: Date, required: true },
  anomalies: { type: Array },
  measures: {
    energy: {
      absorbedActiveEnergy: {
        value: { type: Number },
        quality: { type: String },
      },
      injectedActiveEnergy: {
        value: { type: Number },
        quality: { type: String },
      },
      absorbedInductiveReactiveEnergy: {
        value: { type: Number },
        quality: { type: String },
      },
      injectedInductiveReactiveEnergy: {
        value: { type: Number },
        quality: { type: String },
      },
      absorbedCapacitiveReactiveEnergy: {
        value: { type: Number },
        quality: { type: String },
      },
      injectedCapacitiveReactiveEnergy: {
        value: { type: Number },
        quality: { type: String },
      },
    },
    power: {
      activePower: {
        value: { type: Number },
        quality: { type: String },
      },
      reactivePower: {
        value: { type: Number },
        quality: { type: String },
      },
    },
  },
  setPoint: {
    marketOutcomeId: { type: String },
    activePower: { type: Number },
    reactivePower: { type: Number },
  },
});

const Measurements = mongoose.model('Measurements', measurementsSchema);
module.exports = Measurements;
