const mongoose = require('mongoose');

const { Schema } = mongoose;

const tokenSchema = Schema({
  accessToken: String,
  accessTokenExpiresAt: Date,
  refreshToken: String,
  refreshTokenExpiresAt: Date,
  client: Object,
  user: Object,
});

const Token = mongoose.model('Token', tokenSchema);
module.exports = Token;
