const mongoose = require('mongoose');

const { Schema } = mongoose;

const offerSchema = Schema({
  playerId: { type: String }, // Aggregator ID
  playerServiceId: { type: String }, // Aggregator ServiceId
  marketSession: { type: Schema.Types.ObjectId, ref: 'MarketSession' },
  realTime: { type: Boolean },
  index: { type: Number }, // 0 to 15 or from 0 95
  powerType: { type: String, default: 'active', enum: ['active', 'reactive'] },
  pod: { type: String },
  power: { type: Number },
  price: { type: Number },
});

offerSchema.index({
  pod: 1,
  index: 1,
  marketSession: 1,
  powerType: 1,
}, { unique: true });

const Request = mongoose.model('Offer', offerSchema);
module.exports = Request;
