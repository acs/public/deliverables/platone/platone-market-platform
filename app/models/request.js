const mongoose = require('mongoose');

const { Schema } = mongoose;

const requestSchema = Schema({
  playerId: { type: String }, // DSO/TSO ID
  playerServiceId: { type: String }, // DSO/TSO ServiceId
  marketSession: { type: Schema.Types.ObjectId, ref: 'MarketSession' },
  index: { type: Number }, // 0 to 15 or from 0 95
  serviceType: { type: String, default: 'offer', enum: ['offer', 'DSO_request', 'TSO_request'] },
  powerType: { type: String, default: 'active', enum: ['active', 'reactive'] },
  pod: { type: Array },
  power: { type: Number },
  price: { type: Number },
});

requestSchema.index({
  pod: 1,
  index: 1,
  marketSession: 1,
  powerType: 1,
  playerId: 1,
}, { unique: true });

const Request = mongoose.model('Request', requestSchema);
module.exports = Request;
