const schedule = require('node-schedule');
const settlementLib = require('../libs/settlement');
const marketSessionLib = require('../libs/marketSession');
const marketClearingService = require('../services/marketClearing');
const SettlementService = require('../services/settlement');
const TSOSimulatorService = require('../services/tsoSimulator');
const validatedOutcomeLib = require('../libs/validatedOutcome');

async function createMarketSessionRealTime(marketSessionId) {
  console.log('Start Market Session Real Time Scheduling');
  console.log(`ID MARKET SESSION ${marketSessionId}`);
  for (let i = 0; i < 6; i += 1) {
    const marketSessionRt = {};
    marketSessionRt.marketType = 'realTime';
    const dtStart = new Date();
    marketSessionRt.start = dtStart.setHours(21 + (4 * i), 0, 0);
    const dtStartActivation = new Date(dtStart);
    marketSessionRt.startActivation = dtStartActivation.setHours(dtStart.getHours() + 3, 0, 0);
    const dtEnd = new Date();
    marketSessionRt.end = dtEnd.setHours(22 + (4 * i), 0, 0);
    const dtEndActivation = new Date(dtStart);
    marketSessionRt.endActivation = dtEndActivation.setHours(dtStart.getHours() + 7, 0, 0);
    marketSessionRt.linkedSession = marketSessionId;
    marketSessionRt.status = 'created';
    marketSessionRt.duration = 4;
    marketSessionRt.interval = 15;

    marketSessionLib.save(marketSessionRt).then(() => {
      console.log(`STATUS MARKET SESSION REAL TIME ${marketSessionRt.status}`);
    }).catch((err) => console.log(err));
  }
}

async function createMarketSession() {
  const mSession = await marketSessionLib.getByTypeAndStatus('dayAhead');
  console.log(`MARKET SESSION ${mSession}`);

  if (!mSession) {
    console.log('Create Market Session Scheduling');
    const marketSession = {};
    marketSession.marketType = 'dayAhead';
    const dtStart = new Date();
    marketSession.start = dtStart.setHours(11, 0, 0);
    const dtStartActivation = new Date();
    marketSession.startActivation = dtStartActivation.setHours(dtStart.getHours() + (13), 0, 0);
    const dtEnd = new Date();
    marketSession.end = dtEnd.setHours(15, 30, 0);
    const dtEndActivation = new Date();
    marketSession.endActivation = dtEndActivation.setHours(dtStart.getHours() + (37), 0);
    marketSession.status = 'created';
    marketSession.duration = 24;
    marketSession.interval = 15;

    await marketSessionLib.save(marketSession).then((savedSession) => {
      console.log(`STATUS MARKET SESSION ${savedSession.status}`);
      console.log(`ID MARKET SESSION ${savedSession._id}`);
      createMarketSessionRealTime(savedSession._id);
    }).catch((err) => console.log(err));
  }
}

async function startMarketSession() {
  console.log('Start Market Session Scheduling');
  const marketSession = await marketSessionLib.getToBeStarted();
  if (marketSession) {
    const activeSession = await marketSessionLib.getByType(marketSession.marketType);
    if (!activeSession) {
      const newSession = marketSession;
      newSession.status = 'active';
      console.log(`marketSession START >>${newSession}`);
      marketSessionLib.save(newSession).then(() => {
        console.log(`STATUS MARKET SESSION ${newSession.status}`);
      }).catch((err) => console.log(err));
    } else {
      console.log(`Another Session in market ${marketSession.marketType} is already active`);
    }
  } else {
    console.log('No sessions to be started');
  }
}

async function stopExpiredSessions() {
  console.log('Stop Market Session Scheduling');
  await marketSessionLib.getExpired().then((marketSession) => {
    if (marketSession) {
      const expiredSession = marketSession;
      expiredSession.status = 'closed';
      console.log(`marketSession STOP >>${expiredSession}`);
      marketSessionLib.save(expiredSession).then(() => {
        console.log(`STATUS MARKET SESSION ${expiredSession.status}`);
        console.log(`CLEAR MARKET ${expiredSession}`);
        marketClearingService.match(expiredSession);
      }).catch((err) => console.log(err));
    } else {
      console.log('No expired sessions');
    }
  }).catch((err) => console.log(err));
}

function startSettlement() {
  console.log('Start Settlement Scheduling');
  marketSessionLib.getExpiredByActivation().then(async (marketSession) => {
    if (marketSession) {
      const validatedOutcom = await validatedOutcomeLib.get({ marketSession: marketSession._id });
      console.log(`VALIDATED OUTCOME IN SETTLEMENT ${JSON.stringify(validatedOutcom)}`);
      const expiredActivationSession = marketSession;
      if (validatedOutcom[0]) {
        expiredActivationSession.status = 'settled';
        const settlement = await SettlementService.performSettlement(expiredActivationSession);
        console.log(`SETTLEMENT ${JSON.stringify(settlement)}`);
      } else {
        expiredActivationSession.status = 'empty';
      }
      marketSessionLib.save(expiredActivationSession);
    } else {
      console.log('No sessions to be started');
    }
  });
}

function sendSettlement() {
  console.log('Send Settlement Scheduling');
  settlementLib.getUnsent().then(async (settlement) => {
    if (settlement) {
      const updatedSettlement = settlement;
      let { endActivation } = settlement.marketSession;
      // endActivation = endActivation.setHours(endActivation.getHours() + 1);
      endActivation = endActivation.setMinutes(endActivation.getMinutes() + 10);
      if (new Date() >= new Date(endActivation)) {
        await SettlementService.sendSettlement(settlement.marketSession, settlement);
        updatedSettlement.sent = true;
        settlementLib.save(updatedSettlement);
      }
      console.log('Settlement sent correctly');
    } else {
      console.log('No settlement to send');
    }
  });
}

function tsoSimulation(marketType) {
  console.log('Simulate TSO Request');
  TSOSimulatorService.simulation(marketType);
}

exports.startScheduling = function () {
  const jobCreate = new schedule.Job(() => {
    createMarketSession();
  });

  const jobStart = new schedule.Job(() => {
    startMarketSession();
  });

  const jobEnd = new schedule.Job(() => {
    stopExpiredSessions();
  });

  const jobStartSettlement = new schedule.Job(() => {
    startSettlement();
  });

  const jobSendSettlement = new schedule.Job(() => {
    sendSettlement();
  });

  const jobTSODayAhead = new schedule.Job(() => {
    tsoSimulation('dayAhead');
  });

  const jobTSORealTime = new schedule.Job(() => {
    tsoSimulation('realTime');
  });

  const ruleCreate = new schedule.RecurrenceRule();
  ruleCreate.hour = 10;

  const ruleOneMinute = new schedule.RecurrenceRule();
  ruleOneMinute.minute = new schedule.Range(0, 59, 1);

  const ruleTenMinute = new schedule.RecurrenceRule();
  ruleTenMinute.minute = new schedule.Range(0, 59, 10);

  const ruleTSO1 = new schedule.RecurrenceRule();
  ruleTSO1.hour = 13;

  const ruleTSO2 = new schedule.RecurrenceRule();
  ruleTSO2.hour = 13;
  ruleTSO2.minute = 30;

  jobCreate.schedule(ruleCreate);
  jobStart.schedule(ruleOneMinute);
  jobEnd.schedule(ruleOneMinute);
  jobStartSettlement.schedule(ruleTenMinute);
  jobSendSettlement.schedule(ruleTenMinute);
  jobTSODayAhead.schedule(ruleTSO1);
  jobTSORealTime.schedule(ruleTSO2);
};
