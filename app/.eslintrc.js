module.exports = {
  root: true,
  parserOptions: {
    parser: "babel-eslint",
  },
  env: {
    browser: true,
  },
  extends: ["airbnb"],
  rules: {
    "no-underscore-dangle": "off",
    "linebreak-style": 0,
    "max-len": 0,
    "guard-for-in": 0,
    "no-restricted-syntax": 0,
    quotes: [0, "double"],
  },
};
