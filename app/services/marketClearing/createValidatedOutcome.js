const validatedOutcomeLib = require('../../libs/validatedOutcome');
const marketOutcomeLib = require('../../libs/marketOutcome');
const marketSessionLib = require('../../libs/marketSession');
const matchLib = require('../../libs/match');
const userLib = require('../../libs/user');
const discardedLib = require('../../libs/discarded');
const KafkaHandler = require('../../kafka/kafka-handler');
const config = require('../../kafka/config').prod;
const web3 = require('../../config/utils').init;
const certificationLib = require('../../libs/certification');
const certificationContract = require('../contracts').platoneCertification(web3);
const logLibs = require("../../libs/log");

const kafka = new KafkaHandler(config);

const logParams = {};
logParams.flow = "flow5";
logParams.platform = "MP";
logParams.dateTime = new Date();

const certificationContractAddress = certificationContract.getAddress();
console.log(`certificationContractAddress ${certificationContractAddress}`);

async function sendValidatedOutcome(marketSessionId, updatedValidatedOutcome) {
  // TODO remove additional fields
  console.log('SEND to Kafka');
  console.log(marketSessionId);
  // console.log(updatedValidatedOutcome);
  const outcomes = [];
  const marketSession = await marketSessionLib.get(marketSessionId);
  const marketParticipants = marketSession.participants;

  for (let i = 0; i < marketParticipants.length; i += 1) {
    const marketParticipantId = marketParticipants[i].playerId;
    if (!outcomes[marketParticipantId]) {
      outcomes[marketParticipantId] = JSON.parse(
        JSON.stringify(updatedValidatedOutcome),
      );
    }
    for (
      let j = 0;
      j < outcomes[marketParticipantId].flexibility.length;
      j += 1
    ) {
      const flexibility = outcomes[marketParticipantId].flexibility[j];

      for (let k = 0; k < flexibility.power.length; k += 1) {
        const power = flexibility.power[k];
        if (
          (marketParticipants[i].playerType === "aggregator"
            && power.playerOfferId !== marketParticipantId)
          || ((marketParticipants[i].playerType === "dso"
            || marketParticipants[i].playerType === "tso")
            && (!power.playerRequestId
              || power.playerRequestId !== marketParticipantId))
        ) {
          flexibility.power.splice(k, 1);
          k -= 1;
        }
      }
    }
    console.log(`VALIDATED -> ${marketParticipantId}`);
    // console.log(JSON.stringify(outcomes[marketParticipantId]));
    const topic = `validated_outcome_${marketParticipantId}`;
    const message = outcomes[marketParticipantId];
    kafka.runProducer(topic, message);

    logParams.type = "message_received";
    logParams.referenceId = outcomes[marketParticipantId].marketOutcome._id;

    logLibs.save(logParams);
    // insert log in mongo message_sent reference=marketOutcomeId flow 5
  }
}

module.exports = (technicalOutcome) => new Promise((resolve, reject) => {
  // insert log in mongo start_execution reference=marketOutcomeId flow 5

  logParams.type = "start_execution";
  logParams.referenceId = technicalOutcome.marketOutcome._id;

  logLibs.save(logParams);

  marketOutcomeLib
    .get({ _id: technicalOutcome.marketOutcome })
    .then(async (marketOutcome) => {
      const validatedPods = [];
      const { marketSession } = marketOutcome[0];
      for (const f in technicalOutcome.flexibility) {
        for (const p in technicalOutcome.flexibility[f].power) {
          const power = technicalOutcome.flexibility[f].power[p];

          // console.log('TECHNICAL POWER');
          // console.log(power);

          // eslint-disable-next-line no-await-in-loop
          const match = await matchLib.get({
            marketSession,
            "acceptedOffers.pod": technicalOutcome.flexibility[f].pod,
            index: power.index,
          });
          /* Recupero eventuali discarded attiva/reattiva */
          // eslint-disable-next-line no-await-in-loop
          const discarded = await discardedLib.setValidated({
            marketSession,
            pod: technicalOutcome.flexibility[f].pod,
            index: power.index,
          });

          // console.log('MATCH');
          // console.log(match);

          // console.log('DISCARDED');
          // console.log(discarded);

          if (match && match.length > 0) {
            for (let i = 0; i < match[0].acceptedOffers.length; i += 1) {
              if (
                match[0].acceptedOffers[i].pod
                === technicalOutcome.flexibility[f].pod
              ) {
                power.playerServiceId = match[0].acceptedOffers[i].playerServiceId;
                power.playerOfferId = match[0].acceptedOffers[i].playerId;
                power.pOffered = Math.round(match[0].acceptedOffers[i].power * 1000) / 1000;
                power.acceptedPMarket = Math.round(match[0].acceptedOffers[i].acceptedPower * 1000)
                  / 1000;
              }
            }
            power.playerRequestId = match[0].playerId;
            power.acceptedPtype = "";
            const acceptedPValue = Math.round(power.acceptedPValue * 1000) / 1000;

            if (power.pOffered !== power.acceptedPMarket) {
              power.acceptedPtype = "Partial COM";
            } else if (
              power.pOffered.toFixed(2) !== acceptedPValue.toFixed(2)
            ) {
              power.acceptedPtype = "Partial TEC";
            } else {
              power.acceptedPtype = "OK";
            }

            if (power.acceptedP === "OK") {
              power.acceptedPPrice = match[0].acceptedPrice;
            } else if (power.acceptedP === "KO") {
              power.rejectionTypeP = "TEC";
            }
            if (power.acceptedQ === "OK") {
              power.acceptedQPrice = match[0].acceptedPrice;
            } else if (power.acceptedQ === "KO") {
              power.rejectionTypeQ = "TEC";
            }
          }

          if (discarded) {
            power.playerServiceId = discarded.playerServiceId;
            power.playerOfferId = discarded.playerId;
            if (discarded.powerType === "active") {
              power.rejectionTypeP = "COM";
            } else {
              power.rejectionTypeQ = "COM";
            }
          }
        }
        // eslint-disable-next-line no-await-in-loop
        const otherDiscarded = await discardedLib.get({
          marketSession,
          pod: technicalOutcome.flexibility[f].pod,
          validated: false,
        });

        validatedPods.push(technicalOutcome.flexibility[f].pod);

        // console.log('ALTRI SCARTATI');
        // console.log(otherDiscarded);

        const discardedMap = [];
        for (const i in otherDiscarded) {
          const discarded = otherDiscarded[i];
          if (discarded) {
            if (!discardedMap[discarded.index]) {
              discardedMap[discarded.index] = {};
            }
            discardedMap[discarded.index].playerServiceId = discarded.playerServiceId;
            discardedMap[discarded.index].playerOfferId = discarded.playerId;
            discardedMap[discarded.index].index = discarded.index;

            if (discarded.powerType === "active") {
              discardedMap[discarded.index].rejectionTypeP = "COM";
            } else {
              discardedMap[discarded.index].rejectionTypeQ = "COM";
            }
          }
        }
        for (const j in discardedMap) {
          technicalOutcome.flexibility[f].power.push(discardedMap[j]);
        }
      }

      const totallyDiscarded = await discardedLib.get({
        marketSession,
        validated: false,
        pod: { $nin: validatedPods },
      });

      // console.log('TOTALLY DISCARDED');
      // console.log(totallyDiscarded);

      const totallyDiscardedMap = [];
      for (let i = 0; i < totallyDiscarded.length; i += 1) {
        const { pod } = totallyDiscarded[i];
        if (!totallyDiscardedMap[pod]) {
          totallyDiscardedMap[pod] = [];
        }
        const newdiscardedPower = {
          index: totallyDiscarded[i].index,
          playerServiceId: totallyDiscarded[i].playerServiceId,
          playerOfferId: totallyDiscarded[i].playerId,
        };
        if (totallyDiscarded[i].powerType === "active") {
          newdiscardedPower.rejectionTypeP = "COM";
        } else {
          newdiscardedPower.rejectionTypeQ = "COM";
        }
        totallyDiscardedMap[pod].push(newdiscardedPower);
      }

      for (const d in totallyDiscardedMap) {
        technicalOutcome.flexibility.push({
          pod: d,
          power: totallyDiscardedMap[d],
        });
      }

      const updatedValidatedOutcome = await validatedOutcomeLib.save(
        technicalOutcome,
      );
      console.log("UPDATED");
      // insert log in mongo end_execution reference=marketOutcomeId flow 5

      logParams.type = "end_execution";
      logParams.referenceId = technicalOutcome.marketOutcome._id;

      logLibs.save(logParams);

      sendValidatedOutcome(marketSession, updatedValidatedOutcome);
      /** ******* START CERTIFICATION ******** */
      try {
        const hash = web3.utils.soliditySha3(updatedValidatedOutcome);
        const user = await userLib.getByUsername("admin");
        console.log(`USER -> ${JSON.stringify(user)}`);
        console.log(`ACCOUNT -> ${JSON.stringify(user.account)}`);

        certificationContract
          .connect(certificationContractAddress)
          .then(async () => {
            const myContract = certificationContract.getContract().contract;

            const tx = {
              from: user.account.address,
              to: certificationContractAddress,
              gas: 24130,
              data: myContract.methods
                .energySnapshot(Date.now(), hash)
                .encodeABI(),
            };
            console.log(`TRANSACTION ${tx}`);
            console.log(`ACCOUNT ${user.account.privateKey}`);
            web3.eth.accounts
              .signTransaction(tx, user.account.privateKey)
              .then((signedTx) => {
                const sentTx = web3.eth.sendSignedTransaction(
                  signedTx.raw || signedTx.rawTransaction,
                );
                sentTx.on("receipt", (receipt) => {
                  console.log(`CERTIFICATION>>>${JSON.stringify(receipt)}`);
                  const certification = {
                    createdAt: Date.now(),
                    owner: user.account.address,
                    hashedData: hash,
                    transactionHash: receipt.transactionHash,
                  };
                  certificationLib.save(certification);
                });
                sentTx.on("error", (err) => {
                  console.log(`CERTIFICATION2>>>${JSON.stringify(err)}`);
                  const certification = {
                    createdAt: Date.now(),
                    owner: user.account.address,
                    hashedData: hash,
                    transactionHash: JSON.parse(JSON.stringify(err)).receipt
                      .transactionHash,
                  };
                  certificationLib.save(certification);
                });
              })
              .catch((err) => {
                console.log(err);
              });
          });
      } catch (error) {
        console.log(error);
      }

      /** ******* END CERTIFICATION ******** */

      resolve(updatedValidatedOutcome);
    })
    .catch((err) => {
      reject(err);
    });
});
