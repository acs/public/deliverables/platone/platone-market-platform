const Holidays = require('date-holidays');
const requestLibs = require('../../libs/request');
const offerLibs = require('../../libs/offer');
const discardedLibs = require('../../libs/discarded');
const matchLib = require('../../libs/match');
const podLib = require('../../libs/pod');
const logLibs = require("../../libs/log");

const createMarketOutcome = require('./createMarketOutcome');

function isHoliday(date) {
  const hd = new Holidays('IT');
  if (hd.isHoliday(date) || date.getDay() === 0) {
    return 'holiday';
  } if (date.getDay() === 6 || hd.isHoliday(date.setDate(date.getDate() + 1))) {
    return 'preholiday';
  }
  return 'workday';
}

async function getPodBaseline(podId, index, powerType) {
  const pod = await podLib.get({ pod: podId });
  const pt = powerType === 'active' ? 'p' : 'q';
  const workday = isHoliday(new Date());
  // console.log('CALCULATE POD BASELINE');
  const baseline = pod[0].powerBaselineCurves[workday][index][pt];
  return baseline;
}

function getBaseline(allPods, requestedPods, index, powerType) {
  let baseline = 0;
  const pt = powerType === 'active' ? 'p' : 'q';
  const workday = isHoliday(new Date());
  // console.log('CALCULATE BASELINE');
  for (let i = 0; i < requestedPods.length; i += 1) {
    for (let j = 0; j < allPods.length; j += 1) {
      if (requestedPods[i] === allPods[j].pod) {
        const pod = allPods[j];
        baseline += pod.powerBaselineCurves[workday][index][pt];
      }
    }
  }
  console.log(`PODS Requested: ${requestedPods}`);
  console.log(`Baseline for index: ${index} and powerType: ${powerType} is: ${baseline}`);
  return baseline;
}

async function clearingAlgorithm(requests) {
  const promises = [];

  const allPods = await podLib.getAll();

  for (let r = 0; r < requests.length; r += 1) {
    const request = requests[r];
    request.acceptedOffers = [];

    const { offers } = request;
    offers.sort((a, b) => a.price - b.price);
    // console.log(`REQUEST ->${request}`);
    const baseline = getBaseline(allPods, request.pod, request.index, request.powerType);

    let powerRequested = request.power - baseline;
    const priceRequested = request.price;
    // console.log(`POWER REQUESTED ->${powerRequested}`);

    for (let o = 0; o < offers.length; o += 1) {
      const podBaseline = getBaseline(allPods, [offers[o].pod], offers[o].index, request.powerType);
      const powerOffered = offers[o].power - podBaseline;
      if (powerRequested !== 0) {
        // console.log(`POWER OFFERED ->${powerOffered}`);
        // TODO abs value
        if (((powerRequested > 0 && powerOffered > 0 && powerOffered <= powerRequested)
          || (powerRequested < 0 && powerOffered < 0 && powerOffered >= powerRequested))
          && offers[o].price <= priceRequested) {
          offers[o].acceptedPower = offers[o].power;
          requests[r].acceptedOffers.push(offers[o]);
          powerRequested -= powerOffered;
        } else if (((powerRequested > 0 && powerOffered > 0)
          || (powerRequested < 0 && powerOffered < 0))
          && offers[o].price <= priceRequested) {
          offers[o].acceptedPower = offers[o].power - powerOffered + powerRequested;
          requests[r].acceptedOffers.push(offers[o]);
          powerRequested = 0;
        } else if (powerOffered !== 0) {
          offers[o].validated = false;
          discardedLibs.save(offers[o]);
        }
        request.acceptedPrice = offers[o].price;
      } else if (powerOffered !== 0) {
        offers[o].validated = false;
        discardedLibs.save(offers[o]);
      }
    }
    promises.push(matchLib.save(request));
  }

  Promise.all(promises).then((matches) => {
    if (matches[0]) createMarketOutcome(matches[0].marketSession);
  });
}

async function saveDiscarded(offers) {
  for (let o = 0; o < offers.length; o += 1) {
    const offer = offers[o];
    // eslint-disable-next-line no-await-in-loop
    const podBaseline = await getPodBaseline(offer.pod, offer.index, offer.powerType);
    // console.log(podBaseline);
    const powerOffered = offer.power - podBaseline;
    // console.log(console.log(`POWER OFFERED ->${powerOffered}`));
    if (powerOffered !== 0) {
      offer.validated = false;
      // console.log(`DISCARDED OFFER ->${offer}`);
      discardedLibs.save(offer);
    }
  }
}

function matchingAlgorithm(requests, offers) {
  // Find matches between requests and offers with the same timestamp/power

  // insert log in mongo start_execution flow 3 reference = marketOutcomeId (da settare alla end_execution)

  const logParams = {};
  logParams.type = "start_execution";
  logParams.flow = "flow3";
  logParams.platform = "MP";
  logParams.dateTime = new Date();

  logLibs.save(logParams);

  for (let r = 0; r < requests.length; r += 1) {
    const request = requests[r];
    request.offers = [];

    for (let o = 0; o < offers.length; o += 1) {
      const offer = offers[o];
      if (
        request.pod.indexOf(offer.pod) !== -1
        && request.index === offer.index
        && request.powerType === offer.powerType
      ) {
        request.offers.push(offer);
        offers.splice(o, 1);
        o -= 1;
      }
    }
  }

  console.log("MATCHES");
  // console.log(JSON.stringify(requests));

  console.log("DISCARDED");
  // console.log(JSON.stringify(offers));

  saveDiscarded(offers);
  clearingAlgorithm(requests);
}

function calculateIndexes(hourStart) {
  if (hourStart === 21) {
    return { start: 0, end: 15 };
  }
  if (hourStart === 1) {
    return { start: 16, end: 31 };
  }
  if (hourStart === 5) {
    return { start: 32, end: 47 };
  }
  if (hourStart === 9) {
    return { start: 48, end: 63 };
  }
  if (hourStart === 13) {
    return { start: 64, end: 79 };
  }
  return { start: 80, end: 95 };
}

module.exports = async (marketSession) => {
  console.log('START CLEARING ALG');
  console.log(marketSession._id);

  let requests = [];
  let offers = [];

  requests = await requestLibs.get({ marketSession: marketSession._id });

  if (marketSession.marketType === 'realTime') {
    const dtStart = new Date(marketSession.start);
    const hourStart = dtStart.getHours();
    const indexes = calculateIndexes(hourStart);
    console.log(indexes);
    // Retrieve all the offers of the DayAhead with real time TRUE
    offers = await offerLibs.get({ marketSession: marketSession.linkedSession, realTime: true, index: { $gte: indexes.start, $lte: indexes.end } });

    // Retrieve all the matched offers of the DayAhead with real time FALSE
    for (let i = indexes.start; i <= indexes.end; i += 1) {
      // eslint-disable-next-line no-await-in-loop
      const offersMatch = await matchLib.get({ marketSession: marketSession.linkedSession, index: i.toString() });
      for (let index = 0; index < offersMatch.length; index += 1) {
        const match = offersMatch[index];
        for (let j = 0; j < match.acceptedOffers.length; j += 1) {
          const acceptedOffer = match.acceptedOffers[j];
          if (acceptedOffer.realTime === false) {
            offers.push(acceptedOffer);
          }
        }
      }
    }
    const promises = [];
    for (let k = 0; k < offers.length; k += 1) {
      const offer = offers[k];
      offer.playerServiceId += '-realTime';
      offer.marketSession = marketSession._id;
      delete offer._id;
      promises.push(offerLibs.save(offer));
    }
    try {
      await Promise.all(promises);
    } catch (e) {
      console.log(e.message);
    }
  }

  offers = await offerLibs.get({ marketSession: marketSession._id });

  console.log('OFFER');
  // console.log(offers);
  matchingAlgorithm(requests, offers);
};
