const utils = require('../../utils');
const matchLib = require('../../libs/match');
const marketOutcomeLib = require('../../libs/marketOutcome');
const logLibs = require("../../libs/log");

module.exports = (marketSessionId) => {
  matchLib.get({ marketSession: marketSessionId }).then((matches) => {
    console.log('RECUPERO RICHIESTE MATCHATE');
    // console.log(matches);

    const marketOutcome = {
      marketSession: matches[0].marketSession._id,
      startTime: matches[0].marketSession.startActivation,
      marketType: matches[0].marketSession.marketType,
      duration: matches[0].marketSession.duration,
      interval: matches[0].marketSession.interval,
      flexibility: [],
    };

    const pods = [];

    for (const r in matches) {
      const match = matches[r];

      for (const o in match.acceptedOffers) {
        const acceptedOffer = match.acceptedOffers[o];

        if (!pods[acceptedOffer.pod]) {
          pods[acceptedOffer.pod] = [];
        }

        if (!pods[acceptedOffer.pod][match.index]) {
          pods[acceptedOffer.pod][match.index] = {};
        }

        if (match.powerType === 'active') {
          pods[acceptedOffer.pod][match.index].index = match.index;
          pods[acceptedOffer.pod][match.index].p = acceptedOffer.acceptedPower;
          pods[acceptedOffer.pod][match.index].priorityP = 1;
        } else {
          pods[acceptedOffer.pod][match.index].index = match.index;
          pods[acceptedOffer.pod][match.index].q = acceptedOffer.acceptedPower;
          pods[acceptedOffer.pod][match.index].priorityQ = 1;
        }
      }
    }

    for (const p in pods) {
      marketOutcome.flexibility.push({
        pod: p,
        power: pods[p],
      });
    }

    console.log('MARKET OUTCOME');
    // console.log(JSON.stringify(marketOutcome));

    marketOutcomeLib.save(marketOutcome).then(async (result) => {
      console.log("MARKET OUTCOME SAVED");
      // console.log(result);

      // insert log in mongo end_execution reference = marketOutcomeId

      const logParams = {};
      logParams.type = "end_execution";
      logParams.flow = "flow3";
      logParams.platform = "MP";
      logParams.referenceId = result._id;
      logParams.dateTime = new Date();

      logLibs.save(logParams);

      // TODO send market outcome to DSOTP
      const validatingFlexibilities = {
        marketOutcomeId: result._id,
        playerId: "Market",
        startTime: result.startTime,
        marketType: result.marketType,
        duration: result.duration,
        interval: result.interval,
        flexibilities: result.flexibility,
      };

      console.log("VALIDATING FLEX");
      // console.log(JSON.stringify(validatingFlexibilities));
      const url = "addValidatingFlexibilities";
      const client = utils.MarketRequest();
      await client.post(url, validatingFlexibilities);
      // insert log in mongo message_sent flow3

      logParams.type = "message_sent";
      logLibs.save(logParams);
    }).catch((err) => {
      console.log(err);
    });
  });
};
