const match = require('./matchFlexibilityServices');
const createMarketOutcome = require('./createMarketOutcome');
const createValidatedOutcome = require('./createValidatedOutcome');

const self = {
  match,
  createValidatedOutcome,
  createMarketOutcome,
};

module.exports = self;
