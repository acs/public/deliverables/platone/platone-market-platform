const Holidays = require('date-holidays');
const measurementsLibs = require('../../libs/measurements');
const Settlement = require('../../models/settlement');
const validateOutcomeLibs = require('../../libs/validatedOutcome');
const podLib = require('../../libs/pod');
const tokenisation = require('./tokenisation');

function findTimeSlot(day, index) {
  console.log('find time');
  console.log(day);
  console.log(index);
  return new Date(new Date(day).setHours(0, (Number(index) + 1) * 15, 0, 0));
}

function isHoliday(date) {
  const hd = new Holidays('IT');
  if (hd.isHoliday(date) || date.getDay() === 0) {
    return 'holiday';
  } if (date.getDay() === 6 || hd.isHoliday(date.setDate(date.getDate() + 1))) {
    return 'preholiday';
  }
  return 'workday';
}

async function getBaseline(podId, index, powerType) {
  const pod = await podLib.get({ pod: podId });
  const pt = powerType === 'active' ? 'p' : 'q';
  const workday = isHoliday(new Date());
  // console.log('CALCULATE BASELINE');
  const baseline = pod[0].powerBaselineCurves[workday][index][pt];
  return baseline;
}

module.exports = async (marketSession) => {
  console.log('START SETTLEMENT');

  const validatedOutcome = await validateOutcomeLibs.get({ marketSession });
  // console.log(JSON.stringify(validatedOutcome));

  const newSettlement = new Settlement({
    startTime: validatedOutcome[0].startTime,
    marketOutcome: validatedOutcome[0].marketOutcome,
    marketSession: validatedOutcome[0].marketSession,
    marketType: validatedOutcome[0].marketType,
    flexibility: [],
  });

  const pods = [];

  for (let i = 0; i < validatedOutcome[0].flexibility.length; i += 1) {
    const flexibility = validatedOutcome[0].flexibility[i];

    for (let j = 0; j < flexibility.power.length; j += 1) {
      const power = flexibility.power[j];
      if ((power.acceptedPValue && power.acceptedPtype) || (power.acceptedQValue && power.acceptedQtype)) {
        console.log(`VERIFY ${power.acceptedPValue}`);
        // console.log(power);
        const dataTime = findTimeSlot(validatedOutcome[0].startTime, power.index);
        // console.log(dataTime);
        // eslint-disable-next-line no-await-in-loop
        const podMeasurement = await measurementsLibs.get({ pod: flexibility.pod, dataTime });
        console.log('MEASURE');
        console.log(JSON.stringify(podMeasurement));
        if (!pods[flexibility.pod]) {
          pods[flexibility.pod] = [];
        }
        const podSettlement = {
          index: power.index,
          requestedP: null,
          measuredP: null,
          paidP: null,
          tokenP: null,
          requestedQ: null,
          measuredQ: null,
          paidQ: null,
          tokenQ: null,
          requestPlayerId: power.playerRequestId,
          offerPlayerId: power.playerOfferId,
        };
        if (podMeasurement[0]) {
          if (podMeasurement[0].setPoint.activePower && power.acceptedPtype) {
            podSettlement.requestedP = podMeasurement[0].setPoint.activePower;
            podSettlement.measuredP = podMeasurement[0].measures.power.activePower.value;

            // eslint-disable-next-line no-await-in-loop
            const activeBaseline = await getBaseline(flexibility.pod, power.index, 'active');
            const deltaOfferedActive = Number(podMeasurement[0].setPoint.activePower - activeBaseline);
            const deltaMeasuredActive = Number(podMeasurement[0].measures.power.activePower.value - activeBaseline);
            /* console.log(`activeBaseline ${activeBaseline}`);
            console.log(`deltaOfferedActive ${deltaOfferedActive}`);
            console.log(`deltaMeasuredActive ${deltaMeasuredActive}`); */
            let deltaP = 0;

            if (deltaOfferedActive < 0 && deltaMeasuredActive < 0) {
              deltaP = Math.max(
                Number(deltaOfferedActive),
                Number(deltaMeasuredActive),
              );
            } else if (deltaOfferedActive > 0 && deltaMeasuredActive > 0) {
              deltaP = Math.min(
                Number(deltaOfferedActive),
                Number(deltaMeasuredActive),
              );
            }

            // console.log(`DELTAP ${deltaP}`);
            podSettlement.paidP = Math.abs(deltaP * power.acceptedPPrice);
            podSettlement.tokenP = Math.round(podSettlement.paidP * 1000);
          }

          if (podMeasurement[0].setPoint.reactivePower && power.acceptedQtype) {
            podSettlement.requestedQ = podMeasurement[0].setPoint.reactivePower;
            podSettlement.measuredQ = podMeasurement[0].measures.power.reactivePower.value;

            // eslint-disable-next-line no-await-in-loop
            const reactiveBaseline = await getBaseline(flexibility.pod, power.index, 'reactive');
            const deltaOfferedReactive = Number(podMeasurement[0].setPoint.reactivePower - reactiveBaseline);
            const deltaMeasuredReactive = Number(podMeasurement[0].measures.power.reactivePower.value - reactiveBaseline);

            let deltaQ = 0;

            if (deltaOfferedReactive < 0 && deltaMeasuredReactive < 0) {
              deltaQ = Math.max(
                Number(deltaOfferedReactive),
                Number(deltaMeasuredReactive),
              );
            } else if (deltaOfferedReactive > 0 && deltaMeasuredReactive > 0) {
              deltaQ = Math.min(
                Number(deltaOfferedReactive),
                Number(deltaMeasuredReactive),
              );
            }

            // console.log(deltaQ);
            podSettlement.paidQ = Math.abs(deltaQ * power.acceptedQPrice);
            podSettlement.tokenQ = Math.round(podSettlement.paidQ * 1000);
          }
          pods[flexibility.pod].push(podSettlement);
        } else {
          if (power.acceptedPValue) {
            podSettlement.measuredP = 0;
            podSettlement.paidP = 0;
            podSettlement.requestedP = power.acceptedPValue;
          }
          if (power.acceptedQValue) {
            podSettlement.measuredQ = 0;
            podSettlement.paidQ = 0;
            podSettlement.requestedQ = power.acceptedQValue;
          }

          pods[flexibility.pod].push(podSettlement);
        }
      }
    }
  }

  for (const p in pods) {
    newSettlement.flexibility.push({
      pod: p,
      power: pods[p],
    });
  }
  newSettlement.sent = false;
  await newSettlement.save();
  await tokenisation(marketSession);
  return newSettlement;
};
