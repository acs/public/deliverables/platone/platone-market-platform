const performSettlement = require('./performSettlement');
const tokenisation = require('./tokenisation');
const sendSettlement = require('./sendSettlement');

const self = {
  performSettlement,
  tokenisation,
  sendSettlement,
};

module.exports = self;
