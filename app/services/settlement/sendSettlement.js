const marketSessionLib = require('../../libs/marketSession');
const KafkaHandler = require('../../kafka/kafka-handler');
const config = require('../../kafka/config').prod;

const kafka = new KafkaHandler(config);

module.exports = async (marketSessionId, settlement) => {
  // TODO remove additional fields
  console.log('SEND Settlement to Kafka');
  console.log(marketSessionId);
  // console.log(updatedValidatedOutcome);
  const settlements = [];
  const marketSession = await marketSessionLib.get(marketSessionId);
  const marketParticipants = marketSession.participants;

  for (let i = 0; i < marketParticipants.length; i += 1) {
    const marketParticipantId = marketParticipants[i].playerId;
    if (!settlements[marketParticipantId]) {
      settlements[marketParticipantId] = JSON.parse(JSON.stringify(settlement));
    }
    for (let j = 0; j < settlements[marketParticipantId].flexibility.length; j += 1) {
      const flexibility = settlements[marketParticipantId].flexibility[j];

      for (let k = 0; k < flexibility.power.length; k += 1) {
        const power = flexibility.power[k];
        if ((marketParticipants[i].playerType === 'aggregator' && power.offerPlayerId !== marketParticipantId)
          || ((marketParticipants[i].playerType === 'dso' || marketParticipants[i].playerType === 'tso') && (!power.requestPlayerId || power.requestPlayerId !== marketParticipantId))) {
          flexibility.power.splice(k, 1);
          k -= 1;
        }
      }
    }
    console.log(`SETTLEMENT -> ${marketParticipantId}`);
    const topic = `settlement_${marketParticipantId}`;
    const message = settlements[marketParticipantId];
    kafka.runProducer(topic, message);
  }
};
