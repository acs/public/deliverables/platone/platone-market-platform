const settlementLibs = require('../../libs/settlement');
const web3 = require('../../config/utils').init;
const podLib = require('../../libs/pod');
const tokenContract = require('../contracts').token(web3);

const tokenContractAddress = tokenContract.getAddress();
const truffleConfig = require('../../truffle-config');

module.exports = async (marketSession) => {
  console.log('START TOKENISATION');

  console.log(`MARKET SESSION ${marketSession}`);

  const settlement = await settlementLibs.get({ marketSession });
  console.log(`SETTLEMENT ${JSON.stringify(settlement)}`);

  for (let i = 0; i < settlement[0].flexibility.length; i += 1) {
    const flexibility = settlement[0].flexibility[i];
    const podId = settlement[0].flexibility[i].pod;

    const owner = truffleConfig.networks.psm.from;
    console.log(`owner >> ${owner}`);

    for (let j = 0; j < flexibility.power.length; j += 1) {
      const { tokenP } = flexibility.power[j];
      const { tokenQ } = flexibility.power[j];
      tokenContract.connect(tokenContractAddress).then(async () => {
        const pod = await podLib.get({ pod: podId });
        const to = pod[0].wallet;
        console.log(`to >> ${to}`);
        const amountFrom = await tokenContract.balanceOf(owner);
        const amountTo = await tokenContract.balanceOf(to);
        console.log(`amountFrom >> ${amountFrom}`);
        console.log(`amountTo >> ${amountTo}`);
        let tokens = null;
        if (tokenP) {
          tokens = tokenP;
        }
        if (tokenQ) {
          tokens = tokenQ;
        }
        if (tokens != null) {
          await tokenContract.transfer(owner, to, tokens).then(() => {
            console.log('TOKEN SENT');
          }).catch((err) => {
            console.log(err);
          });
        }
      }).catch((err) => {
        console.log(err);
      });
    }
  }
};
