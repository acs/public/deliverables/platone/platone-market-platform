const simulation = require('./simulation.js');

const self = {
  simulation,
};

module.exports = self;
