const podLib = require('../../libs/pod');
const utils = require('../../utils');

module.exports = async (marketType) => {
  const pods = await podLib.get({ pomId: 'CP-IRLANDESE' });

  const aggregatedCurves = [];

  for (let i = 0; i < pods.length; i += 1) {
    const pod = pods[i];
    // console.log(pod.powerBaselineCurves.workday);
    const baseline = pod.powerBaselineCurves.workday;
    for (let j = 0; j < baseline.length; j += 1) {
      if (!aggregatedCurves[baseline[j].timeSlotIndex]) {
        aggregatedCurves[baseline[j].timeSlotIndex] = 0;
      }
      aggregatedCurves[baseline[j].timeSlotIndex] += baseline[j].p;
    }
  }

  // console.log(aggregatedCurves);

  const date = new Date();

  const newRequest = {
    playerId: 'tso2',
    marketType,
    playerServiceId: `TSO:CP-IRLANDESE:${marketType}-${date}`,
    serviceType: 'TSO_request',
    flexibility: [
      {
        flexibleBlock: {
          availableTimeSlots: [],
        },
        pod: 'CP-IRLANDESE',
        power: [
          {
            index: '74',
            p: aggregatedCurves[74] + 500,
            pPrice: 1,
          },
          {
            index: '75',
            p: aggregatedCurves[75] + 500,
            pPrice: 1,
          },
        ],
      },
    ],
  };

  const url = 'flexibilityService';
  const client = utils.TSORequest();
  await client.post(url, newRequest);
};
