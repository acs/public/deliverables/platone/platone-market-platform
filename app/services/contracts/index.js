const settlement = require('./settlement');
const token = require('./token');
const platoneCertification = require('./certification');
const utils = require('./utils');

const self = {
  settlement,
  token,
  platoneCertification,
  utils,
};

module.exports = self;
