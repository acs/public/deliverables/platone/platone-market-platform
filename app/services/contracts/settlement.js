const truffleContract = require('@truffle/contract');
const artifact = require('../../build/contracts/PlatoneSettlement.json');
const { web3ProviderFix } = require('../../config/utils');

const settlementContract = truffleContract(artifact);

function contract(web3) {
  settlementContract.setProvider(web3.currentProvider);
  web3ProviderFix(settlementContract);
  let myContract;

  const deploy = async (address, day1, day2, day3, day4, day5, day6, day7) => {
    const gasPrice = await web3.eth.getGasPrice();
    myContract = await settlementContract.new(day1, day2, day3, day4, day5, day6, day7, {
      from: address,
      gas: 4712388,
      gasPrice,
    });
    return myContract.address;
  };

  const connect = async (address) => {
    myContract = await settlementContract.at(address);
  };

  const getSettlement = async () => myContract.getSettlement();

  const getAddress = () => {
    const { networks } = settlementContract;
    const addresses = [];
    for (const i in networks) {
      console.log(networks[i]);
      addresses.push(networks[i].address);
    }
    return addresses[addresses.length - 1];
  };

  return {
    connect,
    deploy,
    getSettlement,
    getAddress,
  };
}

module.exports = contract;
