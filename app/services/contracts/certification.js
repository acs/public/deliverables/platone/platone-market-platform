const truffleContract = require('@truffle/contract');
const artifact = require('../../build/contracts/PlatoneCertification.json');
const { web3ProviderFix } = require('../../config/utils');

const certificationContract = truffleContract(artifact);

function contract(web3) {
  certificationContract.setProvider(web3.currentProvider);
  web3ProviderFix(certificationContract);
  // eslint-disable-next-line no-shadow
  let contract;

  const deploy = async (address) => {
    const gasPrice = await web3.eth.getGasPrice();
    contract = await certificationContract.new({
      from: address,
      gas: 4712388,
      gasPrice,
    });
  };

  const connect = async (address) => {
    contract = await certificationContract.at(address);
  };

  const getEnergyAccount = async (owner) => contract.getEnergyAccount({ from: owner });
  const energySnapshot = async (owner, timestamp, value) => {
    console.log(owner);
    console.log(timestamp);
    console.log(value);

    return contract.energySnapshot(timestamp, value, { from: owner, gas: 120000, gasPrice: 1 });
  };

  const getAddress = () => {
    const { networks } = certificationContract;
    const addresses = [];
    for (const i in networks) {
      console.log(networks[i]);
      addresses.push(networks[i].address);
    }
    return addresses[addresses.length - 1];
  };

  const getContract = () => contract;

  return {
    connect,
    deploy,
    energySnapshot,
    getEnergyAccount,
    getAddress,
    getContract,
  };
}

module.exports = contract;
