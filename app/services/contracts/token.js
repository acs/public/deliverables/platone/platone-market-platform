const truffleContract = require('@truffle/contract');
const artifact = require('../../build/contracts/PlatoneToken.json');
const { web3ProviderFix } = require('../../config/utils');

const tokenContract = truffleContract(artifact);

function contract(web3) {
  tokenContract.setProvider(web3.currentProvider);
  web3ProviderFix(tokenContract);
  let myContract;

  const connect = async (address) => {
    myContract = await tokenContract.at(address);
  };

  const transfer = (owner, to, tokens) => {
    console.log(`tokens >>>${tokens}`);
    // const gas = await myContract.methods.transfer.estimateGas(to, tokens);
    // const gasPrice = await web3.eth.getGasPrice();

    // return myContract.transfer(to, tokens, { from: owner, gas, gasPrice });
    return myContract.transfer(to, tokens, { from: owner });
  };

  const transferFrom = async (from, to, tokens) => {
    const gas = await myContract.transferFrom.estimateGas(from, to, tokens);
    // console.log(`GAS${gas}`);
    const gasPrice = await web3.eth.getGasPrice();
    // console.log(`GAS PRICE${gasPrice}`);

    return myContract.transferFrom(from, to, tokens, { from, gas, gasPrice });
  };

  const balanceOf = async (address) => {
    const gas = await myContract.balanceOf.estimateGas(address);
    const gasPrice = await web3.eth.getGasPrice();
    // console.log(myContract);
    const balance = await myContract.balanceOf(address, { address, gas, gasPrice });
    // console.log(balance);
    return balance;
  };

  const totalSupply = async () => {
    const gas = await myContract.totalSupply.estimateGas();
    const gasPrice = await web3.eth.getGasPrice();
    const balance = await myContract.totalSupply({ gas, gasPrice });
    return balance;
  };

  const getAddress = () => {
    const { networks } = tokenContract;
    const addresses = [];
    for (const i in networks) {
      addresses.push(networks[i].address);
    }
    return addresses[addresses.length - 1];
  };

  return {
    connect,
    transfer,
    transferFrom,
    getAddress,
    balanceOf,
    totalSupply,
  };
}

module.exports = contract;
