const readMeasurements = require('./readMeasurements.js');

const self = {
  readMeasurements,
};

module.exports = self;
