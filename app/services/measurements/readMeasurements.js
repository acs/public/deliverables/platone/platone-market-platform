const KafkaHandler = require("../../kafka/kafka-handler");
const measurementsLibs = require("../../libs/measurements");
const logLibs = require("../../libs/log");

const kafkaEnv = process.env.KAFKA_ENV || "scd";
const config = require("../../kafka/config")[kafkaEnv];

module.exports = () => {
  const kafka = new KafkaHandler(config);
  const topic = config.topics;
  const { groupId } = config;

  kafka.runConsumer(topic, groupId, (message) => {
    console.log("received measurement");
    // insert log in mongo message_received reference = pod + dataTime flow 8

    const logParams = {};
    logParams.type = "message_received";
    logParams.flow = "flow8";
    logParams.platform = "MP";
    logParams.referenceId = message.pod.concat("-").concat(message.dataTime);
    logParams.dateTime = new Date();

    logLibs.save(logParams);

    measurementsLibs.save(message);
  });
};
