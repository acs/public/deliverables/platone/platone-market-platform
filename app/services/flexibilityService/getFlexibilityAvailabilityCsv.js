const csvwriter = require('csv-writer');
// const XLSX = require('xlsx');
const flexibilityAvailabilityService = require('./flexibilityAvailabilityService');
const marketSessionLib = require('../../libs/marketSession');

module.exports = async (marketType, status, n) => {
  const createCsvWriter = csvwriter.createObjectCsvWriter;
  const promises = [];
  let marketSessions = [];
  const flexAvailabilities = [];
  /* const marketType = 'dayAhead';
  const status = 'closed';
  const n = 1; */
  marketSessions = await marketSessionLib.getLastNByType({ marketType, status, n });

  console.log(`MARKET SESSIONS ARRAY SIZE ->${marketSessions.length}`);

  for (let i = 0; i < marketSessions.length; i += 1) {
    const marketSession = marketSessions[i]._id;
    promises.push(flexibilityAvailabilityService(marketSession).then((availability) => {
      flexAvailabilities.push(availability);
    }));
  }
  await Promise.all(promises);
  console.log(`RESULT ->${JSON.stringify(flexAvailabilities)}`);

  const csvWriter = createCsvWriter({
    path: 'flexAvailabilities.csv',
    header: [
      // Title of the columns (column_names)
      { id: 'sessionId', title: 'SESSION ID' },
      { id: 'dateStart', title: 'DATE START' },
      { id: 'dateEnd', title: 'DATE END' },
      { id: 'liquidityUp', title: 'LIQUIDITY UP' },
      { id: 'liquidityDown', title: 'LIQUIDITY DOWN' },
    ],
    fieldDelimiter: ';',
  });
  const results = flexAvailabilities;
  const newArr = results.map((obj) => ({ ...obj, dateStart: obj.dateStart.toLocaleString(), dateEnd: obj.dateEnd.toLocaleString() }));
  // Writerecords function to add records
  csvWriter.writeRecords(newArr).then(() => {
    console.log('Data uploaded into csv successfully');
  });
};
