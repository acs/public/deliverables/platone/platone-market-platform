const flexibilityServiceLibs = require('../../libs/flexibilityService');
const updateFlexibilityService = require('./updateFlexibilityService');
const offerLibs = require('../../libs/offer');
const requestLibs = require('../../libs/request');
const podLibs = require('../../libs/pod');
const marketSessionLibs = require('../../libs/marketSession');

/* async function getPods(pomId) {
  const pod = await podLibs.get({ pomId });
  return pod;
} */

function inRange(x, min, max) {
  return ((x - min) * (x - max) <= 0);
}

function verifyIndex(index, marketSession) {
  const dtStart = new Date(marketSession.start);
  const hourStart = dtStart.getHours();

  if (marketSession.marketType === 'realTime') {
    if (hourStart === 21 && inRange(index, 0, 15)) {
      return true;
    }
    if (hourStart === 1 && inRange(index, 16, 31)) {
      return true;
    }
    if (hourStart === 5 && inRange(index, 32, 47)) {
      return true;
    }
    if (hourStart === 9 && inRange(index, 48, 63)) {
      return true;
    }
    if (hourStart === 13 && inRange(index, 64, 79)) {
      return true;
    }
    if (hourStart === 17 && inRange(index, 80, 95)) {
      return true;
    }
  } else {
    return true;
  }
  return false;
}

module.exports = (flexibilityService) => new Promise(async (resolve, reject) => {
  const promises = [];
  const flexibilities = flexibilityService.flexibility;
  const marketSession = await marketSessionLibs.get(flexibilityService.marketSession);

  for (let j = 0; j < flexibilities.length; j += 1) {
    const flexibility = flexibilities[j];
    // TODO check POD
    let podIds = flexibility.pod;
    if (flexibilityService.serviceType === 'TSO_request') {
      // eslint-disable-next-line no-await-in-loop
      const pods = await podLibs.get({ pomId: flexibility.pod });
      podIds = [];
      for (let p = 0; p <= pods.length - 1; p += 1) {
        podIds.push(pods[p].pod);
      }
    }

    for (let p1 = 0; p1 < flexibility.power.length; p1 += 1) {
      if (verifyIndex(flexibility.power[p1].index, marketSession)) {
        console.log('POWER');
        console.log(flexibility.power[p1]);
        // Active Power
        if (flexibility.power[p1].p && flexibility.power[p1].p !== 0) {
          const newActiveService = {
            index: flexibility.power[p1].index,
            marketSession: flexibilityService.marketSession,
            serviceType: flexibilityService.serviceType,
            realTime: flexibility.realTime,
            power: flexibility.power[p1].p,
            price: flexibility.power[p1].pPrice,
            pod: podIds,
            playerId: flexibilityService.playerId,
            playerServiceId: flexibilityService.playerServiceId,
            powerType: 'active',
          };
          if (flexibilityService.serviceType === 'offer') {
            promises.push(offerLibs.save(newActiveService));
          } else {
            console.log(newActiveService);
            promises.push(requestLibs.save(newActiveService));
          }
        }
        // Reactive Power
        if (flexibility.power[p1].q && flexibility.power[p1].q !== 0) {
          const newReactiveService = {
            index: flexibility.power[p1].index,
            marketSession: flexibilityService.marketSession,
            serviceType: flexibilityService.serviceType,
            realTime: flexibility.realTime,
            power: flexibility.power[p1].q,
            price: flexibility.power[p1].qPrice,
            pod: podIds,
            playerId: flexibilityService.playerId,
            playerServiceId: flexibilityService.playerServiceId,
            powerType: 'reactive',
          };
          if (flexibilityService.serviceType === 'offer') {
            promises.push(offerLibs.save(newReactiveService));
          } else {
            promises.push(requestLibs.save(newReactiveService));
          }
        }
      } else {
        const err = new Error();
        err.status = 400;
        err.message = 'Index not valid for this session';
        promises.push(Promise.reject(err));
      }
    }
  }
  Promise.all(promises).then(() => {
    console.log('RESOLVE');
    // No conflicts in saving

    flexibilityServiceLibs.save(flexibilityService).then((data) => resolve(data)).catch((err) => {
      // Confilcts in saving, rollback
      console.log('UPDATE1');
      updateFlexibilityService(flexibilityService);
      reject(err);
    });
  }).catch((err) => {
    // Confilcts in saving, rollback
    console.log('UPDATE2');
    updateFlexibilityService(flexibilityService);
    reject(err);
  });
});
