const Holidays = require('date-holidays');
const requestLibs = require('../../libs/request');
const offerLibs = require('../../libs/offer');
const podLib = require('../../libs/pod');
const marketSessionLib = require('../../libs/marketSession');

function isHoliday(date) {
  const hd = new Holidays('IT');
  if (hd.isHoliday(date) || date.getDay() === 0) {
    return 'holiday';
  } if (date.getDay() === 6 || hd.isHoliday(date.setDate(date.getDate() + 1))) {
    return 'preholiday';
  }
  return 'workday';
}

function getBaseline(allPods, requestedPods, index, powerType) {
  let baseline = 0;
  const pt = powerType === 'active' ? 'p' : 'q';
  const workday = isHoliday(new Date());
  // console.log('CALCULATE BASELINE');
  for (let i = 0; i < requestedPods.length; i += 1) {
    for (let j = 0; j < allPods.length; j += 1) {
      if (requestedPods[i] === allPods[j].pod) {
        const pod = allPods[j];
        baseline += pod.powerBaselineCurves[workday][index][pt];
      }
    }
  }
  // console.log(`PODS Requested: ${requestedPods}`);
  // console.log(`Baseline for index: ${index} and powerType: ${powerType} is: ${baseline}`);
  return baseline;
}

module.exports = async (marketSessionId) => {
  console.log('START FLEXIBILITY AVAILAIBILITY');
  console.log(marketSessionId);

  const marketSession = await marketSessionLib.get(marketSessionId);

  let requests = [];
  let offers = [];
  const arrayOfferUp = [];
  const arrayOfferDown = [];
  const arrayRequestUp = [];
  const arrayRequestDown = [];
  const arrayLiquiditytUp = [];
  const arrayLiquidityDown = [];
  let resObj;

  if (marketSession.marketType === 'dayAhead') {
    let offerUp = 0;
    let offerDown = 0;
    let requestUp = 0;
    let requestDown = 0;
    let liquidityUp = 0;
    let liquidityDown = 0;
    const allPods = await podLib.getAll();
    for (let i = 0; i <= 95; i += 1) {
      // eslint-disable-next-line no-await-in-loop
      offers = await offerLibs.get({ marketSession: marketSessionId, index: i });

      for (let o = 0; o < offers.length; o += 1) {
      //  console.log(`OFFERS ->${JSON.stringify(offers[o])}`);
        const podBaselineOffer = getBaseline(allPods, [offers[o].pod], offers[o].index, offers[o].powerType);
        //   console.log(`POD BASELINE OFFER ->${podBaselineOffer}`);
        //   console.log(`OFFER POWER ->${offers[o].power}`);
        const powerOffered = offers[o].power - podBaselineOffer;
        //   console.log(`POWER OFFERED ->${powerOffered}`);
        if (powerOffered > 0) {
          arrayOfferUp.push(powerOffered);
        } else if (powerOffered < 0) {
          arrayOfferDown.push(powerOffered);
        }
      }

      // eslint-disable-next-line no-await-in-loop
      requests = await requestLibs.get({ marketSession: marketSessionId, index: i });

      for (let r = 0; r < requests.length; r += 1) {
      // console.log(`REQUEST ->${JSON.stringify(requests[r])}`);
        const podBaselineRequest = getBaseline(allPods, requests[r].pod, requests[r].index, requests[r].powerType);
        //  console.log(`POD BASELINE REQUEST ->${podBaselineRequest}`);
        //  console.log(`REQUEST POWER ->${requests[r].power}`);
        const powerRequested = requests[r].power - podBaselineRequest;
        //  console.log(`POWER REQUESTED ->${powerRequested}`);
        if (powerRequested > 0) {
          arrayRequestUp.push(powerRequested);
        } else if (powerRequested < 0) {
          arrayRequestDown.push(powerRequested);
        }
      }

      offerUp = arrayOfferUp.reduce((a, b) => a + b, 0);
      offerDown = arrayOfferDown.reduce((a, b) => a + b, 0);
      requestUp = arrayRequestUp.reduce((a, b) => a + b, 0);
      requestDown = arrayRequestDown.reduce((a, b) => a + b, 0);

      if (requestUp !== 0) {
        liquidityUp = offerUp / requestUp;
      }
      if (requestDown !== 0) {
        liquidityDown = offerDown / requestDown;
      }
      // console.log(`LIQUIDITY -->> UP: ${liquidityUp} DOWN: ${liquidityDown}`);

      arrayLiquiditytUp.push(liquidityUp);
      arrayLiquidityDown.push(liquidityDown);
    }

    const rsLiquidityUp = arrayLiquiditytUp.reduce((a, b) => a + b, 0);
    const rsLiquidityDown = arrayLiquidityDown.reduce((a, b) => a + b, 0);

    const percLiquidityUp = (rsLiquidityUp * 100) / 96;
    const percLiquidityDown = (rsLiquidityDown * 100) / 96;

    console.log(`PERCENTAGE LIQUIDITY UP ->${percLiquidityUp}`);
    console.log(`PERCENTAGE LIQUIDITY DOWN ->${percLiquidityDown}`);

    resObj = {
      sessionId: marketSessionId,
      dateStart: marketSession.startActivation,
      dateEnd: marketSession.endActivation,
      liquidityUp: percLiquidityUp.toFixed(2),
      liquidityDown: percLiquidityDown.toFixed(2),
    };
  }
  return resObj;
};
