const offerLibs = require('../../libs/offer');
const requestLibs = require('../../libs/request');

module.exports = (flexibilityService) => new Promise((resolve, reject) => {
  if (flexibilityService.serviceType === 'offer') {
    offerLibs.remove(flexibilityService.playerServiceId).then((data) => {
      resolve(data);
    }).catch((err) => reject(err));
  } else {
    requestLibs.remove(flexibilityService.playerServiceId).then((data) => {
      resolve(data);
    }).catch((err) => reject(err));
  }
});
