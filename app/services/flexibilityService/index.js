const createFlexibilityService = require('./createFlexibilityService');
const updateFlexibilityService = require('./updateFlexibilityService');
const flexibilityAvailabilityService = require('./flexibilityAvailabilityService');
const getFlexibilityAvailabilityCsv = require('./getFlexibilityAvailabilityCsv');

const self = {
  createFlexibilityService,
  updateFlexibilityService,
  flexibilityAvailabilityService,
  getFlexibilityAvailabilityCsv,
};

module.exports = self;
