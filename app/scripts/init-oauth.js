const mongoose = require('mongoose');

const mongo = process.env.DATABASE_URL || '161.27.206.176:27017';
mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${mongo}/marketplatform`);
const OauthClient = require('../models/oauthClient');

const oauthClient1 = new OauthClient();
const oauthClient2 = new OauthClient();
const oauthClient3 = new OauthClient();

oauthClient1.clientId = '8b361dac1cf5d4ce650432f82995273b';
oauthClient1.clientSecret = '112b73e120e9f4be7291095d31d77ae956cb34fdd75c2164cf2369693650aeec';
oauthClient1.grants = ['client_credentials'];
oauthClient1.redirectUris = [];

oauthClient2.clientId = '6c812580c16656be13518dbd16bdb9f0';
oauthClient2.clientSecret = '460f65ac41a4dfcec95be2b7396766a5e57fce116e40f23606b73cc84034696c';
oauthClient2.grants = ['client_credentials'];
oauthClient2.redirectUris = [];

oauthClient3.clientId = '1a64d285b69c2c2604176eaeee707210';
oauthClient3.clientSecret = 'a493f5c4763fc9b4a6b74e31f487c7201b4c6afa0a9f66dd45045cb8ab503def';
oauthClient3.grants = ['client_credentials'];
oauthClient3.redirectUris = [];

const promises = [];

promises.push(oauthClient1.save());
promises.push(oauthClient2.save());
promises.push(oauthClient3.save());

Promise.all(promises).then((result) => {
  console.log(result);
  mongoose.connection.close();
}).catch((err) => {
  console.log(err);
  mongoose.connection.close();
});
