const csvwriter = require('csv-writer');
const mongoose = require('mongoose');

const mongo = process.env.DATABASE_URL || '161.27.206.176:27017';
mongoose.Promise = global.Promise;
const marketSessionLib = require('../libs/marketSession');

const flexibilityServiceLibs = require('../libs/flexibilityService');

const createCsvWriter = csvwriter.createObjectCsvWriter;
let flexibilityService = [];
const dsoRequests = [];

function findTimeSlot(day, index) {
  return new Date(new Date(day).setHours(0, (Number(index)) * 15, 0, 0));
}

function mapSession(session) {
  if (session.marketType === 'dayAhead') {
    return '1';
  }
  const startHour = new Date(session.start).getHours();
  return Math.floor(startHour / 4) + 1;
}

async function getDsoRequests() {
  mongoose.connect(`mongodb://${mongo}/marketplatform`);
  flexibilityService = await flexibilityServiceLibs.get({ serviceType: 'DSO_request', createdAt: { $gte: new Date('2022-01-01'), $lte: new Date('2022-12-31') } });
  console.log(flexibilityService.length);
  for (let i = 0; i < flexibilityService.length; i += 1) {
    const f = flexibilityService[i];
    for (let j = 0; j < f.flexibility.length; j += 1) {
      const flexibility = f.flexibility[j];
      // TODO check POD
      const { pod } = flexibility;
      if (flexibility.power) {
        for (let p1 = 0; p1 < flexibility.power.length; p1 += 1) {
          // Active Power
          if (flexibility.power[p1].p && flexibility.power[p1].p !== 0) {
            const newRequest = {
              index: flexibility.power[p1].index,
              startActivation: f.marketSession.start,
              interval: f.interval,
              createdAt: f.createdAt,
              marketType: f.marketType,
              marketSession: f.marketSession,
              realTime: f.realTime,
              power: flexibility.power[p1].p,
              price: flexibility.power[p1].pPrice,
              pod,
              playerId: f.playerId,
              playerServiceId: f.playerServiceId,
              powerType: 'active',
            };
            dsoRequests.push(newRequest);
          }
        }
      }
    }
  }
  const csvWriter = createCsvWriter({
    path: 'areti_market_flexibility_DSO_requests_2022.csv',
    header: [
      // Title of the columns (column_names)
      { id: 'createdAt', title: 'Date' },
      { id: 'timestamp', title: 'Timestamp' },
      { id: 'playerServiceId', title: 'Req_id' },
      { id: 'startActivation', title: 'Strating_Time' },
      { id: 'interval', title: 'Duration' },
      { id: 'marketType', title: 'Market_Type' },
      { id: 'marketSession', title: 'Market_Session' },
      { id: 'marketSession.flexibilityServiceType', title: 'Flexibility_Service_Type' },
      { id: 'power', title: 'Quantity_Requested' },
      { id: 'pod', title: 'Pod_List' },
      { id: 'note', title: 'Note' },
    ],
    fieldDelimiter: ';',
  });
  const results = dsoRequests;
  console.log(dsoRequests);
  const newArr = results.map((obj) => ({
    ...obj, marketSession: mapSession(obj.marketSession), power: obj.power.toFixed(2), createdAt: obj.createdAt.toLocaleString(), startActivation: findTimeSlot(obj.startActivation, obj.index).toLocaleString(),
  }));

  // Writerecords function to add records
  csvWriter.writeRecords(newArr).then(() => {
    console.log('Data uploaded into csv successfully');
  // console.log(`RESULT ->${JSON.stringify(bids)}`);
  });
  mongoose.connection.close();
}

getDsoRequests();
