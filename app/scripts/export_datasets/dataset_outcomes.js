const csvwriter = require('csv-writer');
const mongoose = require('mongoose');

const mongo = process.env.DATABASE_URL || '161.27.206.176:27017';
mongoose.Promise = global.Promise;

const validatedOutocomeLibs = require('../libs/validatedOutcome');

const createCsvWriter = csvwriter.createObjectCsvWriter;
let validatedOutcomes = [];
const outcomes = [];

function findTimeSlot(day, index) {
  return new Date(new Date(day).setHours(0, (Number(index)) * 15, 0, 0));
}

function mapSession(session) {
  if (session.marketType === 'dayAhead') {
    return '1';
  }
  const startHour = new Date(session.start).getHours();
  return Math.floor(startHour / 4) + 1;
}

async function getBids() {
  mongoose.connect(`mongodb://${mongo}/marketplatform`);
  validatedOutcomes = await validatedOutocomeLibs.get({ startTime: { $gte: new Date('2022-01-01'), $lte: new Date('2022-06-30') } });
  console.log(validatedOutcomes.length);
  for (let i = 0; i < validatedOutcomes.length; i += 1) {
    const f = validatedOutcomes[i];
    for (let j = 0; j < f.flexibility.length; j += 1) {
      const flexibility = f.flexibility[j];
      // TODO check POD
      const { pod } = flexibility;
      if (flexibility.power) {
        for (let p1 = 0; p1 < flexibility.power.length; p1 += 1) {
          // Active Power
          if (flexibility.power[p1].acceptedPValue && flexibility.power[p1].acceptedPValue !== 0) {
            const newOutcome = {
              index: flexibility.power[p1].index,
              startActivation: f.startTime,
              interval: f.marketSession.interval,
              createdAt: f._id.getTimestamp(),
              marketType: f.marketType,
              marketSession: f.marketSession,
              marketOutcome: f.marketOutcome,
              realTime: f.realTime,
              power: flexibility.power[p1].acceptedPValue,
              price: flexibility.power[p1].acceptedPPrice,
              pod,
              playerId: f.playerId,
              playerServiceId: f.playerServiceId,
              powerType: 'active',
            };
            outcomes.push(newOutcome);
          }
        }
      }
    }
  }
  const csvWriter = createCsvWriter({
    path: 'areti_market_flexibility_outcomes_2022_1.csv',
    header: [
      // Title of the columns (column_names)
      { id: 'createdAt', title: 'Date' },
      { id: 'timestamp', title: 'Timestamp' },
      { id: 'marketOutcome', title: 'Market_Outcome_Id' },
      { id: 'startActivation', title: 'Strating_Time' },
      { id: 'marketType', title: 'Market_Type' },
      { id: 'marketSession', title: 'Market_Session' },
      { id: 'marketSession.flexibilityServiceType', title: 'Flexibility_Service_Type' },
      { id: 'power', title: 'Quantity_Accepted' },
      { id: 'pod', title: 'Pod' },
      { id: 'note', title: 'Note' },
    ],
    fieldDelimiter: ';',
  });
  const results = outcomes;
  // console.log(outcomes);
  const newArr = results.map((obj) => ({
    ...obj, marketSession: mapSession(obj.marketSession), power: obj.power.toFixed(2), createdAt: obj.createdAt.toLocaleString(), startActivation: findTimeSlot(obj.startActivation, obj.index).toLocaleString(),
  }));

  // Writerecords function to add records
  csvWriter.writeRecords(newArr).then(() => {
    console.log('Data uploaded into csv successfully');
  // console.log(`RESULT ->${JSON.stringify(bids)}`);
  });
  mongoose.connection.close();
}

getBids();
