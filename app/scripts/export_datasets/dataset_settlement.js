const Holidays = require('date-holidays');
const csvwriter = require('csv-writer');
const mongoose = require('mongoose');

const mongo = process.env.DATABASE_URL || '161.27.206.176:27017';
mongoose.Promise = global.Promise;

const settlementLibs = require('../libs/settlement');
const podLibs = require('../libs/pod');

const createCsvWriter = csvwriter.createObjectCsvWriter;
let settlements = [];
const outcomes = [];

function isHoliday(date) {
  const hd = new Holidays('IT');
  if (hd.isHoliday(date) || date.getDay() === 0) {
    return 'holiday';
  } if (date.getDay() === 6 || hd.isHoliday(date.setDate(date.getDate() + 1))) {
    return 'preholiday';
  }
  return 'workday';
}

async function getPodBaseline(podId, index, powerType) {
  const pod = await podLibs.get({ pod: podId });
  if (pod[0]) {
    return pod[0].powerBaselineCurves;
    /* const pt = powerType === 'active' ? 'p' : 'q';
    const workday = isHoliday(new Date());
    // console.log('CALCULATE POD BASELINE');
    const baseline = pod[0].powerBaselineCurves[workday][index][pt];
    return baseline; */
  }
  return null;
}

async function getBids() {
  mongoose.connect(`mongodb://${mongo}/marketplatform`);
  settlements = await settlementLibs.get({ startTime: { $gte: new Date('2022-07-01'), $lte: new Date('2022-12-31') } });
  console.log(settlements.length);
  for (let i = 0; i < settlements.length; i += 1) {
    const f = settlements[i];
    for (let j = 0; j < f.flexibility.length; j += 1) {
      const flexibility = f.flexibility[j];
      // TODO check POD
      const { pod } = flexibility;
      // eslint-disable-next-line no-await-in-loop
      const baseline = await getPodBaseline(pod);
      if (flexibility.power) {
        for (let p1 = 0; p1 < flexibility.power.length; p1 += 1) {
          // Active Power
          if (flexibility.power[p1].requestedP && flexibility.power[p1].requestedP !== 0) {
            const workday = isHoliday(new Date());
            const newOutcome = {
              index: flexibility.power[p1].index,
              createdAt: f._id.getTimestamp(),
              marketSession: f.marketSession,
              marketOutcome: f.marketOutcome,
              acceptedPower: flexibility.power[p1].requestedP,
              pod,
            };
            if (flexibility.power[p1].measuredP) {
              newOutcome.deliveredPower = flexibility.power[p1].measuredP.toFixed(2);
            }
            if (baseline && baseline[workday][flexibility.power[p1].index].p) {
              newOutcome.baseline = baseline[workday][flexibility.power[p1].index].p.toFixed(2);
            }
            outcomes.push(newOutcome);
          }
        }
      }
    }
  }
  const csvWriter = createCsvWriter({
    path: 'areti_market_flexibility_settlement_2022_2.csv',
    header: [
      // Title of the columns (column_names)
      { id: 'createdAt', title: 'Date' },
      { id: 'timestamp', title: 'Timestamp' },
      { id: 'marketOutcome', title: 'Market_Outcome_Id' },
      { id: 'pod', title: 'Pod' },
      { id: 'acceptedPower', title: 'Quantity_Accepted' },
      { id: 'deliveredPower', title: 'Quantity_Delivered' },
      { id: 'baseline', title: 'Baseline' },
      { id: 'note', title: 'Note' },
    ],
    fieldDelimiter: ';',
  });
  const results = outcomes;
  // console.log(outcomes);
  const newArr = results.map((obj) => ({
    ...obj, acceptedPower: obj.acceptedPower.toFixed(2), createdAt: obj.createdAt.toLocaleString(),
  }));

  // Writerecords function to add records
  csvWriter.writeRecords(newArr).then(() => {
    console.log('Data uploaded into csv successfully');
  // console.log(`RESULT ->${JSON.stringify(bids)}`);
  });
  mongoose.connection.close();
}

getBids();
