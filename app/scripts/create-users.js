const mongoose = require('mongoose');

const mongo = process.env.DATABASE_URL || 'localhost:27017';
mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${mongo}/marketplatform`);
const User = require('../models/user');

const dso = new User();
const tso = new User();
const agg = new User();

dso.username = 'dso1';
dso.password = dso.generateHash('dso1');
dso.role = 'DSO';

tso.username = 'tso1';
tso.password = tso.generateHash('tso1');
tso.role = 'TSO';

agg.username = 'agg1';
agg.password = agg.generateHash('agg1');
agg.role = 'aggregator';

const promises = [];

promises.push(dso.save());
promises.push(tso.save());
promises.push(agg.save());

Promise.all(promises).then((result) => {
  console.log(result);
  mongoose.connection.close();
}).catch((err) => {
  console.log(err);
  mongoose.connection.close();
});
