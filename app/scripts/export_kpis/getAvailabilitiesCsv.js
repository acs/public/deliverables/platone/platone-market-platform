const mongoose = require('mongoose');
const FlexibilityService = require('../../services/flexibilityService');

const mongo = process.env.DATABASE_URL || '161.27.206.176:27017';
// const mongo = process.env.DATABASE_URL || '127.0.0.1:27017';
mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${mongo}/marketplatform`);

FlexibilityService.getFlexibilityAvailabilityCsv('realTime', 'settled', 70).then((data) => {
  console.log(`TEST AVAILABITY ->${data}`);
  // mongoose.connection.close();
});
