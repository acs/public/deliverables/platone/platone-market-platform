const mongoose = require('mongoose');
const ContractUtil = require('../services/contracts').utils();

const mongo = process.env.DATABASE_URL || 'localhost:27017';
mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${mongo}/marketplatform`);
const User = require('../models/user');

const newUser = new User();

const account = ContractUtil.newWallet();
console.log(account);

newUser.username = 'admin';
newUser.password = newUser.generateHash('Energysecur2023!');
newUser.role = 'admin';
newUser.account = account;

newUser.save().then((result) => {
  console.log(result);
  mongoose.connection.close();
}).catch((err) => {
  console.log(err);
  mongoose.connection.close();
});
