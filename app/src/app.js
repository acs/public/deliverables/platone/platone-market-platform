const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const passport = require('passport');
const mongoose = require('mongoose');
const swaggerUi = require('swagger-ui-express');

const routes = require('../routes');
const marketPlatformScheduler = require('../jobs/marketPlatformScheduler');
const measurementsService = require('../services/measurements');

const app = express();

process.env.TZ = 'Europe/Rome';

const mongo = process.env.DATABASE_URL || 'localhost:27017';
mongoose.Promise = global.Promise;
mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);
mongoose.set('useCreateIndex', true);
mongoose.connect(`mongodb://${mongo}/marketplatform`);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', () => {
  console.log('Connection Succeeded');
});

// Load API documentation
const swaggerDocument = require('../openapi');

const swaggerOptions = {
  customSiteTitle: 'Platone Market Platform OpenAPI Docs',
};

app.use(morgan('default'));
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded());
app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ limit: '5mb', extended: true }));
app.use(cors());
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
require('../config/passport')(passport);

// routes ==================================================
app.use('/', routes);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, swaggerOptions));

// Background processes
marketPlatformScheduler.startScheduling();
measurementsService.readMeasurements();

const port = process.env.PORT || 18081;
app.listen(port);

console.log(`Welcome to Platone Market Platorm Application listening on port ${port}`);
