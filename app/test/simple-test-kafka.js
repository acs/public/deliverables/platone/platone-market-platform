const KafkaHandler = require('../kafka/kafka-handler');
const config = require('../kafka/config').dev;

const kafka = new KafkaHandler(config);

const topic = 'validated_outcome_DSO';
const message = 'message';
kafka.runProducer(topic, message);
