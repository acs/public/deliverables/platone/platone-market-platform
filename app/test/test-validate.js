const KafkaHandler = require('../kafka/kafka-handler');
const config = require('../kafka/config').dev;

const kafka = new KafkaHandler(config);

const topic = 'validated_outcome_ACEAE';
const message = {
  __v: 0,
  duration: 2,
  flexibility: [
    {
      pod: 'IT002E1111111A',
      power: [
        {
          index: '43',
          acceptedQValue: 6.85,
          acceptedPValue: 12.7,
          playerServiceId: 'off1',
          playerOfferId: 'ACEAE',
          playerRequestId: 'areti',
          acceptedPPrice: 10000,
          acceptedQPrice: 10000,
        },
        {
          index: '44',
          acceptedPValue: 12,
          acceptedQValue: 6.4995,
          playerServiceId: 'off1',
          playerOfferId: 'ACEAE',
          playerRequestId: 'areti',
          rejectionTypeP: 'TEC',
          acceptedQPrice: 10000,
        },
      ],
    },
  ],
  interval: 15,
  marketOutcome: '609e79f17bb18800202a448a',
  marketSession: '609e73237bb18800202a3da2',
  marketType: 'dayAhead',
  startTime: '2021-06-03T22:00:00.000Z',
};
kafka.runProducer(topic, message);
