const Holidays = require('date-holidays');

function isHoliday(date) {
  const hd = new Holidays('IT');
  if (hd.isHoliday(date) || date.getDay() === 0) {
    return 'holiday';
  } if (date.getDay() === 6 || hd.isHoliday(date.setDate(date.getDate() + 1))) {
    return 'preholiday';
  }
  return 'workday';
}

console.log(isHoliday(new Date('2021-12-26')));
