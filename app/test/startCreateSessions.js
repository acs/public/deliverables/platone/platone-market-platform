const mongoose = require('mongoose');
const MarketPlatformScheduler = require('../jobs/marketPlatformScheduler');

const mongo = process.env.DATABASE_URL || 'localhost:27017';
mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${mongo}/marketplatform`);

MarketPlatformScheduler.createMarketSession().then(() => {
  console.log('ADDING SESSIONS');
  // mongoose.connection.close();
});
