const json = {
  playerId: 'agg1',
  marketType: 'dayAhead',
  playerServiceId: 'o2',
  serviceType: 'offer',
  flexibility: [
    {
      pod: '1',
      power: [
        {
          index: 0,
          p: 5, // discarded
          pPrice: 3,
          q: 7, // discarded
          qPrice: 3,
        },
        {
          index: 52,
          p: 3, // invece di 2 ( offerta +1) match
          pPrice: 2,
        },
      ],
    },
    {
      pod: '2',
      power: [
        {
          index: 52,
          p: 2, // invece di 1.1 (offerta +0.9) match - ma non comprato per prezzo
          pPrice: 3,
          q: 1,
          qPrice: 3, // discarded
        }, {
          index: 53,
          p: 3, // invece di 1.5 (offerta +1.5) match ma non comprato per power
          pPrice: 3,
        },
        {
          index: 54,
          p: 3, // invece di 1.7 (offerta +1.3) match parziale di 0.3
          pPrice: 2,
        },
        {
          index: 55,
          p: 2, // invece di 1.8 (offerta +0.2) match ma non comprato per power
          pPrice: 2,
        },
      ],
    },
    {
      pod: '3',
      power: [
        {
          index: 52,
          p: 3, // invece di 0.9 ( offerta +2.1) match - non comprato per prezzo
          pPrice: 6,
        },
        {
          index: 54,
          p: 3, // invece di 2.1 (offerta +0.9) match
          pPrice: 1,
        },
      ],
    },
  ],
};

console.log(JSON.stringify(json));
