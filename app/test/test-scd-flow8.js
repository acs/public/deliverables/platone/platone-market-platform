const KafkaHandler = require('../kafka/kafka-handler');
const config = require('../kafka/config').scd;

const kafka = new KafkaHandler(config);

const topic = 'flow-8';
const groupId = 'market-flow-8';

kafka.runConsumer(topic, groupId);
