const mongoose = require('mongoose');
const SettlementService = require('../services/settlement');

// const mongo = process.env.DATABASE_URL || '161.27.206.176:27017';
const mongo = process.env.DATABASE_URL || '161.27.206.176:27017';
mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${mongo}/marketplatform`);

SettlementService.performSettlement('62bc06806cb38c00200c740d').then((data) => {
  console.log(data);
  mongoose.connection.close();
});
