const KafkaHandler = require('../kafka/kafka-handler');
const config = require('../kafka/config').dev;

const kafka = new KafkaHandler(config);

/* const topic = 'validated_outcome_DSO';
const groupId = 'market-flow-8'; */

const topic = 'settlement_ACEAE';
const groupId = 'group - 0';

kafka.runConsumer(topic, groupId, (res) => {
  console.log(JSON.stringify(res));
});
