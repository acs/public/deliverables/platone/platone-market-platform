const mongoose = require('mongoose');

const validatedOutcomeLib = require('../libs/validatedOutcome');
const marketSessionLib = require('../libs/marketSession');

const KafkaHandler = require('../kafka/kafka-handler');
const config = require('../kafka/config').dev;

const kafka = new KafkaHandler(config);

const mongo = process.env.DATABASE_URL || '161.27.206.176:27017';
mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${mongo}/marketplatform`);

validatedOutcomeLib.getAll().then(async (results) => {
  const validatedOutcome = results[results.length - 1];
  console.log('SEND to Kafka');
  const outcomes = [];
  const marketSession = await marketSessionLib.get('60d1a58978e4d2002bdcd197');
  const marketParticipants = marketSession.participants;

  for (let i = 0; i < marketParticipants.length; i += 1) {
    const marketParticipantId = marketParticipants[i].playerId;
    if (!outcomes[marketParticipantId]) {
      outcomes[marketParticipantId] = JSON.parse(JSON.stringify(validatedOutcome));
    }
    for (let j = 0; j < outcomes[marketParticipantId].flexibility.length; j += 1) {
      const flexibility = outcomes[marketParticipantId].flexibility[j];

      if (marketParticipants[i].playerType === 'aggregator' && flexibility.power[0].playerOfferId !== marketParticipantId) {
        outcomes[marketParticipantId].flexibility.splice(j, 1);
        j -= 1;
      } else if ((marketParticipants[i].playerType === 'dso' || marketParticipants[i].playerType === 'tso')) {
        for (let k = 0; k < flexibility.power.length; k += 1) {
          const power = flexibility.power[k];
          if (!power.playerRequestId || power.playerRequestId !== marketParticipantId) {
            flexibility.power.splice(k, 1);
            k -= 1;
          }
        }
      }
    }
    console.log(`VALIDATED -> ${marketParticipantId}`);
    console.log(JSON.stringify(outcomes[marketParticipantId]));
    const topic = `validated_outcome_${marketParticipantId}`;
    const message = outcomes[marketParticipantId];
    kafka.runProducer(topic, message);
  }
}).catch((err) => {
  console.log(err);
});
