const json = {
  playerId: 'agg1',
  marketType: 'dayAhead',
  playerServiceId: 'o1',
  serviceType: 'offer',
  flexibility: [
    {
      pod: '1',
      power: [
        {
          index: 0,
          p: 5, // match 1  - ma non comprato
          pPrice: 3,
          q: 7, // discarded
          qPrice: 3,
        },
        {
          index: 1,
          p: 10, // match 2 - ma non comprato
          pPrice: 8,
        },
      ],
    },
    {
      pod: '2',
      power: [
        {
          index: 0,
          p: 2,
          pPrice: 5, // match 1  - ma non comprato
          q: 1,
          qPrice: 3, // discarded
        }, {
          index: 1,
          p: 3, // match 2
          pPrice: 3,
          q: 8, // match 3
          qPrice: 7,
        },
        {
          index: 2,
          p: 10, // // match 4  - ma non comprato
          pPrice: 8,
        },
        {
          index: 10,
          p: 10, // discarded
          pPrice: 8,
        },
      ],
    },
    {
      pod: '3',
      power: [
        {
          index: 0,
          p: 11, // match 1
          pPrice: 1,
        },
        {
          index: 10,
          p: 10, // discarded
          pPrice: 8,
        },
      ],
    },
  ],
};
