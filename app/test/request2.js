const json = {
  playerId: 'dso1',
  marketType: 'dayAhead',
  playerServiceId: 'r2',
  serviceType: 'DSO_request',
  flexibility: [
    {
      pod: [
        '1',
        '2',
        '3',
      ],
      power: [
        {
          index: 52,
          p: 5, // invece di 4 - richiesta +1
          pPrice: 3,
        },
        {
          index: 53,
          p: 6.5, // uguale alla baseline
          pPrice: 3,
        },
        {
          index: 54,
          p: 7, // invece di 4.8 - richiesta +1.2
          pPrice: 3,
        },
        {
          index: 55,
          p: 5, // invece di 5.5 - richiesta -0,5
          pPrice: 3,
        },
      ],
    },
  ],
};

console.log(JSON.stringify(json))