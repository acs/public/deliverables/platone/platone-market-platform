const mongoose = require('mongoose');

const TsoSimulator = require('../services/tsoSimulator');

const mongo = process.env.DATABASE_URL || '161.27.206.176:27017';
mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${mongo}/marketplatform`);
TsoSimulator.simulation('dayAhead');
