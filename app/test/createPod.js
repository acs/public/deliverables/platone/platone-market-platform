const mongoose = require('mongoose');

const mongo = process.env.DATABASE_URL || 'localhost:27017';
mongoose.Promise = global.Promise;

const Pod = require('../models/pod');
const baselines = require('./baselines.json');

async function createPods() {
  mongoose.connect(`mongodb://${mongo}/marketplatform`);
  for (const key in baselines) {
    const newPod = new Pod({
      pod: key,
      aggregatorId: 'agg1',
      smartContractId: '605dad42cd1a422b7009456f',
      zone: 'Zone 1',
      pom: '1',
      powerBaselineCurves: {
        workday: baselines[key],
        preholiday: baselines[key],
        holiday: baselines[key],
      },
      maxFlexibility: {
        upperP: 12,
        downP: 0,
        upperQ: 12,
        downQ: 0,
      },
    });

    // const savedPod = await newPod.save();
    // console.log(`Saved new Pod ->${savedPod}`);
    console.log(JSON.stringify(newPod));
  }
  mongoose.connection.close();
}

createPods(baselines);
