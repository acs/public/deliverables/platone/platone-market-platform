const json = {
  playerId: 'dso1',
  marketType: 'dayAhead',
  playerServiceId: 'r1',
  serviceType: 'DSO_request',
  flexibility: [
    {
      pod: [
        '1',
        '2',
        '3',
      ],
      power: [
        {
          index: 0,
          p: 5,
          pPrice: 3,
        },
        {
          index: 1,
          p: 5,
          pPrice: 3,
          q: 10,
          qPrice: 7,
        },
        {
          index: 2,
          p: 5,
          pPrice: 3,
        },
        {
          index: 3,
          p: 5,
          pPrice: 3,
        },
      ],
    },
  ],
};
