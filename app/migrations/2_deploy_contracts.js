/* eslint-disable no-undef */
const PlatoneToken = artifacts.require('../contracts/PlatoneToken.sol');
const PlatoneSettlement = artifacts.require('../contracts/PlatoneSettlement.sol');
const PlatoneCertification = artifacts.require('../contracts/PlatoneCertification.sol');

const timeframe = {
  intervals: [0, 1],
  prices: [2, 2],
  types: ['fixed,', 'fixed'],
};

const timeframe2 = {
  intervals: [0, 1],
  prices: [3, 3],
  types: ['fixed,', 'fixed'],
};

module.exports = async (deployer) => {
  await deployer.deploy(PlatoneToken, { gas: 5141445, gasPrice: 1 });
  await deployer.deploy(PlatoneSettlement, timeframe, timeframe2, timeframe, timeframe, timeframe2, timeframe, timeframe, { gas: 5141445, gasPrice: 1 });
  await deployer.deploy(PlatoneCertification, { gas: 5047000, gasPrice: 1 });
};
