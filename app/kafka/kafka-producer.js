const { Kafka } = require('kafkajs');

const config = {
  clientId: '1',
  brokers: ['platone.eng.it:9092'],
};
const kafka = new Kafka(config);
const producer = kafka.producer();

const runProducer = async (topic, message) => {
  await producer.connect();
  producer.send({
    topic,
    messages: [{ value: JSON.stringify(message) }],
  }).then((data) => {
    console.log(data);
    console.log(`Message Sent${JSON.stringify(message)}`);
    producer.disconnect();
  }).catch((err) => {
    console.log(err);
    producer.disconnect();
  });
};

module.exports = runProducer;
