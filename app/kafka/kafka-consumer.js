const { Kafka } = require('kafkajs');

const config = {
  clientId: '1001',
  brokers: ['platone.eng.it:9092'],
  connectionTimeout: 3000,
  authenticationTimeout: 1000,
  reauthenticationThreshold: 10000,
};
// 1.Instantiating kafka
const kafka = new Kafka(config);
// 2.Creating Kafka Consumer and passing group ID.
const consumer = kafka.consumer({ groupId: 'group02' });
const runConsumer = async (topic) => {
// 3.Connecting consumer to kafka broker.
  await consumer.connect();
  // 4.Subscribing to a topic in order to receive messages/data.
  await consumer.subscribe({ topic, fromBeginning: true });
  // 5. Sending an action to be handled for each message RECEIVED.
  await consumer.run({
    eachMessage: ({ t, partition, message }) => {
      const buf = Buffer.from(message.value, 'binary');
      const decodedMessage = JSON.parse(buf.toString());
      console.log({ 'Doing something with the message': t, partition, decodedMessage });
    },
  });
};

module.exports = runConsumer;
