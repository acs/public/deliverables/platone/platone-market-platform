const path = require('path');
const fs = require('fs');

const kafkaBroker = process.env.KAFKA_URL || 'platone.eng.it:9092';

module.exports = {
  dev: {
    clientId: 'PLATONE-gateway-flow-5-kafka',
    brokers: [kafkaBroker],
    ssl: {
      rejectUnauthorized: false,
    /* ca: [fs.readFileSync(path.resolve(__dirname, '../test/fullchain.pem'), 'utf-8')],
      key: fs.readFileSync(path.resolve(__dirname, '../test/client.key'), 'utf-8'),
      cert: fs.readFileSync(path.resolve(__dirname, '../test/client.cer'), 'utf-8'), */
    },
    groupId: 'group-0',
    topics: 'validated_outcome_ACEAE',
  },
  prod: {
    clientId: 'PLATONE-gateway-flow-5-kafka',
    brokers: [kafkaBroker],
    groupId: 'group-0',
    topics: 'validated_outcome_ACEAE',
  },
  scd: {
    clientId: 'market-platform',
    brokers: ['broker-0-dev-h2020.giafar.it:9093', 'broker-1-dev-h2020.giafar.it:9093', 'broker-2-dev-h2020.giafar.it:9093'],
    ssl: {
      ca: [fs.readFileSync(path.resolve(__dirname, '../kafka/certs/ca.crt'), 'utf-8')],
      key: fs.readFileSync(path.resolve(__dirname, '../kafka/certs/market.key'), 'utf-8'),
      cert: fs.readFileSync(path.resolve(__dirname, '../kafka/certs/market.cer'), 'utf-8'),
    },
    groupId: 'market-flow-8',
    topics: 'flow-8',
  },
  scdProd: {
    clientId: 'market-platform',
    brokers: ['broker-0-dev-h2020.giafar.it:9093', 'broker-1-dev-h2020.giafar.it:9093', 'broker-2-dev-h2020.giafar.it:9093'],
    ssl: {
      ca: [fs.readFileSync(path.resolve(__dirname, '../kafka/certs/ca.crt'), 'utf-8')],
      key: fs.readFileSync(path.resolve(__dirname, '../kafka/certs/market.key'), 'utf-8'),
      cert: fs.readFileSync(path.resolve(__dirname, '../kafka/certs/market.cer'), 'utf-8'),
    },
    groupId: 'market-prod-flow-8',
    topics: 'flow-8',
  },
};
