const { Kafka } = require('kafkajs');

class KafkaHandler {
  constructor(config) {
    this.client = new Kafka(config);
    console.log('New Kafka client');
  }

  async runProducer(topic, message) {
    const producer = this.client.producer();
    await producer.connect();
    producer.send({
      topic,
      messages: [{ value: JSON.stringify(message) }],
    });
  }

  async runConsumer(topic, groupId, callback, offset) {
    console.log(topic);
    console.log(groupId);
    const consumer = this.client.consumer({ groupId });
    await consumer.connect();
    await consumer.subscribe({ topic, fromBeginning: true });
    await consumer.run({
      eachMessage: ({ t, partition, message }) => {
        const buf = Buffer.from(message.value, 'binary');
        const decodedMessage = JSON.parse(buf.toString());
        console.log({ 'Doing something with the message': t, partition, decodedMessage });
        callback(decodedMessage);
      },
    });
    if (offset || offset === 0) {
      consumer.seek({ topic, partition: 0, offset });
    }
  }
}

module.exports = KafkaHandler;
